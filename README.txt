################################################################################
#                               TRANSFORMER SETUP                              #
################################################################################

Prerequisites:
##############
java compiler >=1.6


Setup:
######
1) Checkout the project as usual.
2) Create a directory called "bin" within the project root directory.
3) Compile the java files:
javac -d bin -sourcepath src src/*/*.java -classpath "lib/meteor-1.5/meteor-1.5.jar"
4) Run the program by:
$ java -cp "lib/meteor-1.5/meteor-1.5.jar:bin" controller.ControlPanel config.properties files.txt 
w/o Meteor:
$ java -cp bin controller.ControlPanel config.properties files.txt
5) The output is directly written to the project root directory.

I integrated four existing MT evaluation metrics in my code (see references below):
BLEU (papineni2002) and PER (tillmann1997) I rewrote myself.
Of TER (snover2008) I modified the data handling in the source code (TERtest) to call it ea-
-sier from inside my code (MyTER.java).
Meteor (denkowski2011) is used from inside MyMeteor class which calls the METEOR API.  


Some important files:
#####################


### INPUT ###
/data/in/files.txt
These files indicate the Bibles that are going to be run by the program. Bibles 
listed in these files need to be avaliable under their corresponding /data/in
directories in tokenized, lemmatized, normalized, POS-tagged version.

/data/in/babelnet_en/*
Contains the synonym-, hypernym-, hyponym-, and cohyponym information necessary 
for the transformation.


### OUTPUT ###
log*.out files contain the operations applied to words in a set-like format.


### CREDITS ####

@inproceedings{denkowski2011,
	title		= {Meteor 1.3: Automatic Metric for Reliable Optimization and Evaluation of Machine Translation Systems},
	author		= {Denkowski, Michael and Lavie, Alon},
	booktitle	= {Proceedings of the Sixth Workshop on Statistical Machine Translation},
	pages		= {85--91},
	year		= {2011},
	organization = {ACL}
}

@inproceedings{papineni2002,
	author		= {Kishore Papineni and Salim Roukos and Todd Ward and Wei-Jing Zhu},
	title		= {Bleu: a Method for Automatic Evaluation of Machine Translation},
	booktitle	= {Proceedings of 40th Annual Meeting of the Association for Computational Linguistics},
	year		= {2002},
	publisher	= {ACL},
	pages		= {311--318}
} 

@misc{snover2008,
	author	= {Snover, Matthew and Dorr, Bonnie and Schwartz, Richard and Micciulla, Linnea and Makhoul, John},
	title	= {TRANSLATION ERROR RATE},
	howpublished = {\url{http://www.cs.umd.edu/%7Esnover/tercom/}},
	year	= {2008}	
}

@inproceedings{tillmann1997,
	title		= {Accelerated DP Based Search for Statistical Translation.},
	author		= {Tillmann, Christoph and Vogel, Stephan and Ney, Hermann and Zubiaga, Arkaitz and Sawaf, Hassan},
	booktitle	= {Proceedings of Eurospeech-97},
	pages		= {2667--2670},
	year		= {1997}
}





