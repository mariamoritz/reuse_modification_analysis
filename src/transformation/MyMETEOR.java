package transformation;

import java.util.LinkedList;

import edu.cmu.meteor.scorer.MeteorConfiguration;
import edu.cmu.meteor.scorer.MeteorScorer;
import edu.cmu.meteor.util.Constants;

public class MyMETEOR {
	
	public LinkedList<Object> getVerseScore(
			LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, String key, MeteorScorer scorer){	
		
		LinkedList<Object> program = new LinkedList<Object>();
		double meteor = meteor(v1, v2, scorer);
		program.add(String.valueOf(meteor));
		return program;
	}
	
	public double meteor(LinkedList<String> v1, LinkedList<String> v2, MeteorScorer scorer){
			
		String str1 = "";
		String str2 = "";
		for (String w : v1){
			str1 = str1 + w + " ";
		}
		str1 = str1.substring(0, str1.length()-1);
		for (String w : v2){
			str2 = str2 + w + " ";
		}
		str2 = str2.substring(0, str2.length()-1);
		
		double score = scorer.getMeteorStats(str2, str1).score;  // v2, v1!!!!!
		score = 1.0 - score;
		// double score = scorer.getMeteorStats("test string", "reference string").score;
		return score;	
	}
	
	public static void main(String[] args){
		
		MeteorConfiguration config = new MeteorConfiguration();
		config.setLanguage("en");
		config.setNormalization(Constants.NORMALIZE_KEEP_PUNCT);
		MeteorScorer scorer = new MeteorScorer(config);
		
		MyMETEOR met = new MyMETEOR();
		LinkedList<String> v1 = new LinkedList<String>();
		v1.add("the"); v1.add("elephant"); v1.add("eats"); v1.add("the"); v1.add("peanut");
		LinkedList<String> v2 = new LinkedList<String>();
		v2.add("the"); v2.add("elephant"); v2.add("eats"); v2.add("the"); v2.add("groundnut");
		double score = met.meteor(v1, v2, scorer);
		
		//double score = scorer.getMeteorStats("the dog is a nice hound", "the dog is a nice dog").score;  // v2, v1!!!!!
		// double score = scorer.getMeteorStats("test string", "reference string").score;
		System.out.println(score);
		
	}

}
