package transformation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class PER {

	public LinkedList<Object> getVerseScore(
			LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, String key){	
		
		LinkedList<Object> program = new LinkedList<Object>();
		double normEdit = normEditDist(v1, v2);
		program.add(String.valueOf(normEdit));
		return program;
	}
	
	public static double normEditDist(LinkedList<String> v1, LinkedList<String> v2){
		
		HashMap<String, Integer> wordlist1 = new HashMap<String, Integer>();
		HashMap<String, Integer> wordlist2 = new HashMap<String, Integer>();
		HashSet<String> globWordlist = new HashSet<String>();
		//create the word dictionary for verse1
		for (String w : v1){
			globWordlist.add(w);
			int val = 0;
			if (wordlist1.containsKey(w))
				val = wordlist1.get(w);
			val++;
			wordlist1.put(w, val);				
		}
		//create the word dictionary for verse2
		for (String w : v2){
			globWordlist.add(w);
			int val = 0;
			if (wordlist2.containsKey(w))
				val = wordlist2.get(w);
			val++;
			wordlist2.put(w, val);				
		}	
		double sum = 0;
		for (String w: globWordlist){
			int no1 = 0;
			int no2 = 0;
			no1 = wordlist1.containsKey(w) ? wordlist1.get(w) : 0;
			no2 = wordlist2.containsKey(w) ? wordlist2.get(w) : 0;
			sum = sum + Math.abs((double)no1-(double)no2);
		}
		double score = (Math.abs(v1.size()-v2.size()) + sum ) / (2.0*(double)v1.size());
		return score;
	}
	
	
	public static void main(String[] args){
				
		PER wer = new PER();
		LinkedList<String> v1 = new LinkedList<String>();
		v1.add("the"); v1.add("dog"); v1.add("is"); v1.add("a"); v1.add("nice"); v1.add("cat");
		LinkedList<String> v2 = new LinkedList<String>();
		v2.add("the"); v2.add("dog"); v2.add("is"); v2.add("a"); v2.add("nice"); v2.add("dog");
		
		LinkedList<Object> ll = wer.getVerseScore(v1, v1, v1, v2, v2, v2, "1");
		for (Object o : ll){
			System.out.println((String)o.toString());
		}
	}
}
