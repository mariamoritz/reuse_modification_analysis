package transformation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class BLEU {


	// can not return an alignment only one score per verse pair
	// int four_gram_v1 = v1.size()-4+1; 
	public LinkedList<Object> getVerseScore(
			LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, String key){	
	
		int n = 1; // also 3 and 4? Dies 1 really work better than 2???
		LinkedList<Object> program = new LinkedList<Object>();
		HashMap<List<String>, Integer> ng1 = ngrams((List<String>)v1, n);
		HashMap<List<String>, Integer> ng2 = ngrams((List<String>)v2, n);
		
		// calculate p_n 
		// v1 = ref; v2 = sys (see thesis: Related work.Paraphrases and parallel text.Methods.BLEU)
		double p_n = (double)getNgramOverlap(ng1, ng2) / (double)getNgramCount(ng2);
		
		/*double d = getNgramOverlap(ng1, ng2);
		double e = getNgramCount(ng2);
		System.out.println(Double.toString(d));
		System.out.println(Double.toString(e));
		System.out.println(Double.toString(p_n));*/
		
		// calculate BP
		double BP = 1; //Math.exp( Math.min( 1-(v1.size()/v2.size()), 1) );
			
		// calculate BLEU
		double sum_bleu = 0.0;
		for (int i = 0; i <= n; i++){
			sum_bleu = sum_bleu + (1.0/(double)n * Math.log(p_n)); 
		}
		double BLEU = 1.0 - BP * Math.exp(sum_bleu);
		
		//Operation o = localTransformation(w1, v1l.get(i), v1n.get(i), v1pos.get(i), w2, v2l.get(j), v2n.get(j), v2pos.get(j), i, j).getFirst();
		program.add(String.valueOf(BLEU));		
		return program;
		
	}
	
	
	//#############################################################################################
	
	public HashMap<List<String>, Integer> ngrams(List<String> verse, int n){		
		HashMap<List<String>, Integer> ngrams = new HashMap<List<String>, Integer>();
		for (int i = 0; i < verse.size()-n+1; i++){
			int val = 0;
			List<String> new_list = verse.subList(i, i+n);
			if (ngrams.containsKey(new_list)){
				val = ngrams.get(new_list);
			}
			val++;
			ngrams.put(new_list, val);
		}	
		return ngrams;
	}
	
	public int getNgramOverlap(HashMap<List<String>, Integer> ngrams1, HashMap<List<String>, Integer> ngrams2){		
		int overlap = 0;
		for (List<String> ll1 : ngrams1.keySet()){
			if (ngrams2.containsKey(ll1)){
				overlap = overlap + Math.min(ngrams1.get(ll1),ngrams2.get(ll1));
			}
		}
		return overlap;
	}
	
	public int getNgramCount(HashMap<List<String>, Integer> ngrams){
		int count = 0;
		for (List<String> ll : ngrams.keySet()){
			count = count + ngrams.get(ll);
		}
		return count;
	}
	
	
	
	public static void main(String[] args){
				
		BLEU bl = new BLEU();
		LinkedList<String> v1 = new LinkedList<String>();
		//v1.add("the"); v1.add("dog"); v1.add("is"); v1.add("a"); v1.add("nice"); v1.add("hound");
		v1.add("And"); v1.add("Elkanah"); v1.add("went"); v1.add("to"); v1.add("Ramah"); v1.add("to"); v1.add("his"); v1.add("house"); v1.add("And"); v1.add("the"); v1.add("boy");
		v1.add("ministered"); v1.add("to"); v1.add("Jehovah"); v1.add("in"); v1.add("the"); v1.add("presence"); v1.add("of"); v1.add("Eli"); v1.add("the"); v1.add("priest");
		
		LinkedList<String> v2 = new LinkedList<String>();
		//v2.add("the"); v2.add("dog"); v2.add("is"); v2.add("a"); v2.add("nice"); v2.add("dog");
		v2.add("And"); v2.add("Elkanah"); v2.add("will"); v2.add("go"); v2.add("to"); v2.add("Ramah"); v2.add("to"); v2.add("his"); v2.add("house"); v2.add("And"); v2.add("the");
		v2.add("boy"); v2.add("was"); v2.add("serving"); v2.add("Jehovah"); v2.add("in"); v2.add("the"); v2.add("face"); v2.add("of"); v2.add("Eli"); v2.add("the"); v2.add("priest");
		//v2.add("blub");	
		
		LinkedList<Object> ll = bl.getVerseScore(v1, v1, v1, v2, v2, v2, "1");
		for (Object o : ll){
			System.out.println((String)o.toString());
		}
	}
	
}






