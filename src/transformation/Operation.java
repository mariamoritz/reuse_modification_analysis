package transformation;

public class Operation {
	
	private int score = 0;	
	private String label = "";	
	private String sourceword = "";
	private String targetword = "";
	private int sourceindex = 0;
	private int targetindex = 0;
	
	public Operation(){
		;
	}
	
	public Operation(String label, int s, String sw, String tw, int s_ix, int t_ix){
		this.label = label;
		this.score = s;
		this.sourceword = sw;
		this.targetword = tw;
		this.sourceindex = s_ix;
		this.targetindex = t_ix;
	}
	
	public int getScore(){
		return this.score;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public String getTargetWord(){
		return this.targetword;
	}
	
	public String getSourceWord(){
		return this.sourceword;
	}
	
	public int getSourceIndex(){
		return this.sourceindex;
	}
	
	public int getTargetIndex(){
		return this.targetindex;
	}
}
