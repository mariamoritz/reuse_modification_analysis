package transformation;

//import java.text.DecimalFormat;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
//import java.util.concurrent.ConcurrentHashMap.KeySetView;

import model.Lexicon;
import controller.OperateText;
import controller.ControlPanel;

public class Transformation {
	
	private HashMap<String, Integer> op_costs = new HashMap<String, Integer>();
	private HashSet<Integer> handledIdexesLeft = new HashSet<Integer>(); // important for combined alignment
	private HashSet<Integer> handledIdexesRight = new HashSet<Integer>(); // important for combined alignment
	private double indexShiftBackupAlignment = .0;
	
	public LinkedList<Object> transformBerkeley(
			LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, LinkedList<String> v1pos, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, LinkedList<String> v2pos, 
			LinkedList<String> align, String key) {	
		op_costs.put("low_editdist", 3); op_costs.put("low_editdist_poschange", 3);
		op_costs.put("syn", 4); op_costs.put("hyper", 5); op_costs.put("hypo", 5); op_costs.put("co-hypo", 6); op_costs.put("fallback", 99); 
		op_costs.put("syn_poschange", 5); op_costs.put("hyper_poschange", 6); op_costs.put("hypo_poschange", 6); op_costs.put("co-hypo_poschange", 7);  // important for caching
				
		LinkedList<Object> program = new LinkedList<Object>();
		LinkedList<Object> extracodePara = new LinkedList<Object>();
		LinkedList<Object> extracodeRemainder = new LinkedList<Object>();
		LinkedList<Object> updatedInput = new LinkedList<Object>();
		HashMap<String, Operation> left_candidates = new HashMap<String, Operation>();
		HashMap<String, Operation> right_candidates = new HashMap<String, Operation>();
		// this is the n*m run with prioritized lists as product();
		if (align != null){
			for (String pair : align){
				String ix_left = pair.split("-")[1];
				String ix_right = pair.split("-")[0];
				int i = Integer.valueOf(ix_left);
				handledIdexesLeft.add(new Integer(i)); // important for combined alignment
				int j = Integer.valueOf(ix_right);
				handledIdexesRight.add(new Integer(j)); // important for combined alignment
				String w1 = "";
				String w2 = "";
				try{
					w1 = v1.get(i);
					w2 = v2.get(j);
				}
				catch(Exception e){
					System.out.println(key);
					e.printStackTrace(System.out);
				}
				
				Operation o = localTransformation(w1, v1l.get(i), v1n.get(i), v1pos.get(i), w2, v2l.get(j), v2n.get(j), v2pos.get(j), i, j).getFirst();
				// handle the multiple alignment
				// derivation #no. can change if derivation comes with POS change, THEN a syn-relation (supported by bigger dictionary) won't be overwritten by derivation > cost are 4 for both
				if ( left_candidates.containsKey(ix_left) ){
					if ( o.getScore() < left_candidates.get(ix_left).getScore() )
						left_candidates.put(ix_left, o);
				}
				else if ( right_candidates.containsKey(ix_right) ){
					if ( o.getScore() < right_candidates.get(ix_right).getScore() )
						left_candidates.put(ix_left, o); // ATTENTION: changed ix_right to ix_left, check if this is correct
				}
				else{
					left_candidates.put(ix_left, o);
					right_candidates.put(ix_right, o);
				}				
			}				
			// calculate averaged index shift
			int idx = 0;
			double shift = .0;
			// because it is symmetric, here is one run of one side enough: fill the program code
			for (String s : left_candidates.keySet()){
				Operation o = left_candidates.get(s);
				idx = idx + (o.getTargetIndex()-o.getSourceIndex());
				program.add(String.valueOf(o.getSourceIndex()) + ":" + String.valueOf(o.getTargetIndex()) + " " + o.getLabel() + "(" + o.getSourceWord() + "," + o.getTargetWord() + ");");
			}
			shift = (double)idx/(double)left_candidates.keySet().size();				
			
			// if combined alignment is true, here must happen something with the leftovers from the Berkeley alignment
			if (ControlPanel.combinedalignment){
				LinkedList<String> v1_new = cutList(v1);
				LinkedList<String> v1l_new = cutList(v1l);
				LinkedList<String> v1n_new = cutList(v1n);
				LinkedList<String> v1pos_new = cutList(v1pos);
				LinkedList<String> v2_new = cutList(v2);
				LinkedList<String> v2l_new = cutList(v2l);
				LinkedList<String> v2n_new = cutList(v2n);
				LinkedList<String> v2pos_new = cutList(v2pos);
				// paraphrases requires combined alignment, because Berkeley already finds multi-word associations
				// if paraphrases, then the input for the remainders is the output from paraphrases
				if (ControlPanel.paraphrases){
					/*for (String s : v1n_new)
						System.out.print(s + " ");
					System.out.println();
					for (String s : v2n_new)
						System.out.print(s);
					System.out.println();*/
					ArrayList<Object> arLi = transformParaphrase(v1, v1l, v1n, v1pos, v2, v2l, v2n, v2pos, key);
					//ArrayList<Object> arLi = transformParaphrase(v1_new, v1l_new, v1n_new, v1pos_new, v2_new, v2l_new, v2n_new, v2pos_new, key);
					extracodePara = (LinkedList<Object>) arLi.get(0);
					updatedInput = (LinkedList<Object>) arLi.get(1);
					for (Object entry : extracodePara)
						program.add(entry);
				// hand over updated data: call transform with the input from transforPara if para is false
				extracodeRemainder = transform((LinkedList<String>)updatedInput.get(0), (LinkedList<String>)updatedInput.get(1), (LinkedList<String>)updatedInput.get(2), (LinkedList<String>)updatedInput.get(3), 
						(LinkedList<String>)updatedInput.get(4), (LinkedList<String>)updatedInput.get(5), (LinkedList<String>)updatedInput.get(6), (LinkedList<String>)updatedInput.get(7), key);
				
				}
				// if not paraphrases, then the remainder input comes directly from the cut lists above
				else{
					extracodeRemainder = transform(v1_new, v1l_new, v1n_new, v1pos_new, v2_new, v2l_new, v2n_new, v2pos_new, key);  
				}
				for (Object entry : extracodeRemainder){
					program.add(entry);
          //System.out.println(entry.toString());
        }
				// combine both shifts from berkeley and remainder when combined is true 
				shift = ((shift + indexShiftBackupAlignment)/2.0)/((v1.size()+v2.size())/2.0);
			}
			
			// return the numeric feature representation instead
			if (ControlPanel.features){
				program = getFeatures(left_candidates, extracodePara, extracodeRemainder);
			}
			// idx shift is an extra entry at the end of the program in either format
			if (ControlPanel.idxshift) program.add(String.valueOf(shift));
			return program;
		}
		else{
			return null;
		}
	}
	
	/***
	 * @param v1 verse one or sentence one
	 * @param v1l lemmatized verse one
	 * @param v2 verse two or sentence two
	 * @param v2l lemmatized verse two
	 * @return List of program lines, each for transforming one word of verse one into a good candidate word from verse two with lowest costs possible.
	 */
	public LinkedList<Object> transform(
			LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, LinkedList<String> v1pos, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, LinkedList<String> v2pos, 
			String key){
		op_costs.put("low_editdist", 3); op_costs.put("low_editdist_poschange", 3);
		op_costs.put("syn", 4); op_costs.put("hyper", 5); op_costs.put("hypo", 5); op_costs.put("co-hypo", 6); op_costs.put("fallback", 99); 
		op_costs.put("syn_poschange", 5); op_costs.put("hyper_poschange", 6); op_costs.put("hypo_poschange", 6); op_costs.put("co-hypo_poschange", 7);  // important for caching
		
		LinkedList<Operation> nopOps = new LinkedList<Operation>();
		LinkedList<Operation> nopPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> lowerOps = new LinkedList<Operation>();
		LinkedList<Operation> lowerPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> normOps = new LinkedList<Operation>();
		LinkedList<Operation> normPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> lemOps = new LinkedList<Operation>();
		LinkedList<Operation> lemPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> derivationOps = new LinkedList<Operation>();
		LinkedList<Operation> derivationPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> loweditdistOps = new LinkedList<Operation>();
		LinkedList<Operation> loweditdistPOSchaOps = new LinkedList<Operation>();
		
		LinkedList<Operation> synOps = new LinkedList<Operation>();
		LinkedList<Operation> synPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> hyperOps = new LinkedList<Operation>();
		LinkedList<Operation> hyperPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> hypoOps = new LinkedList<Operation>();
		LinkedList<Operation> hypoPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> cohypoOps = new LinkedList<Operation>();
		LinkedList<Operation> cohypoPOSchaOps = new LinkedList<Operation>();
		LinkedList<Operation> fallbackOps = new LinkedList<Operation>();

		LinkedList<Object> program = new LinkedList<Object>();
		LinkedList<Operation> allOPs = new LinkedList<Operation>();
		HashSet<Integer> taken1 = new HashSet<Integer>();
		HashSet<Integer> taken2 = new HashSet<Integer>();

		// this is the n*m run with prioritizing lists as product
		for (String w1 : v1){
			int i = v1.indexOf(w1);
			for (String w2 : v2){
				int j = v2.indexOf(w2);
				// TODO: this possible needs to be modified in order to suit multiple operation trees?  
				/* ignore? LinkedList<Operation> cheap_ops = this.getMinOp(word_op_trees);	String line = ""; for (Operation o : cheap_ops){line = line + "\t" + o.getLabel();} tmp_program_line = line;*/
				Operation o = localTransformation(w1, v1l.get(i), v1n.get(i), v1pos.get(i), w2, v2l.get(j), v2n.get(j), v2pos.get(j), i, j).getFirst(); // CAREFUL! DOES NOT SUPPOR MORPH OPS YET
				if (o.getLabel().equals("NOP")) nopOps.add(o);
				else if (o.getLabel().equals("NOP_poschange")) nopPOSchaOps.add(o);
				else if (o.getLabel().equals("lower")) lowerOps.add(o);
				else if (o.getLabel().equals("lower_poschange")) lowerPOSchaOps.add(o);
				else if (o.getLabel().equals("norm")) normOps.add(o);
				else if (o.getLabel().equals("norm_poschange")) normPOSchaOps.add(o);
				else if (o.getLabel().equals("lem")) lemOps.add(o);
				else if (o.getLabel().equals("lem_poschange")) lemPOSchaOps.add(o);
				else if (o.getLabel().equals("derivation")) derivationOps.add(o);
				else if (o.getLabel().equals("derivation_poschange")) derivationPOSchaOps.add(o);
				else if (o.getLabel().equals("low_editdist")) loweditdistOps.add(o);
				else if (o.getLabel().equals("low_editdist_poschange")) loweditdistPOSchaOps.add(o);	
				else if (o.getLabel().equals("syn")) synOps.add(o);
				else if (o.getLabel().equals("syn_poschange")) synPOSchaOps.add(o);
				else if (o.getLabel().equals("hyper")) hyperOps.add(o);
				else if (o.getLabel().equals("hyper_poschange")) hyperPOSchaOps.add(o);
				else if (o.getLabel().equals("hypo")) hypoOps.add(o);
				else if (o.getLabel().equals("hypo_poschange")) hypoPOSchaOps.add(o);
				else if (o.getLabel().equals("cohypo")) cohypoOps.add(o);
				else if (o.getLabel().equals("cohypo_poschange")) cohypoPOSchaOps.add(o);
				else if (o.getLabel().equals("fallback")) fallbackOps.add(o);
			}
		}
		allOPs.addAll(nopOps); allOPs.addAll(nopPOSchaOps);
		allOPs.addAll(lowerOps); allOPs.addAll(lowerPOSchaOps);
		allOPs.addAll(normOps); allOPs.addAll(normPOSchaOps);
		allOPs.addAll(lemOps); allOPs.addAll(lemPOSchaOps);
		allOPs.addAll(derivationOps); allOPs.addAll(derivationPOSchaOps);
		allOPs.addAll(loweditdistOps); allOPs.addAll(loweditdistPOSchaOps);
		allOPs.addAll(synOps); allOPs.addAll(synPOSchaOps);
		allOPs.addAll(hyperOps); allOPs.addAll(hyperPOSchaOps);
		allOPs.addAll(hypoOps); allOPs.addAll(hypoPOSchaOps); 
		allOPs.addAll(cohypoOps); allOPs.addAll(cohypoPOSchaOps); 
		allOPs.addAll(fallbackOps);
			
		// here, make use of the operations in a prioritized order
		int idx = 0;
		int idx_counter = 0;
		for (Operation op : allOPs){
			if ( !taken1.contains(op.getSourceIndex()) && !taken2.contains(op.getTargetIndex()) ){
				taken1.add(op.getSourceIndex());
				if (!op.getLabel().equals("fallback"))
					taken2.add(op.getTargetIndex());
				// update averaged index shift
				idx = idx + (op.getTargetIndex()-op.getSourceIndex());
				idx_counter++;
				if(ControlPanel.features)
					program.add(op);
				else
					program.add(String.valueOf(op.getSourceIndex()) + ":" + String.valueOf(op.getTargetIndex()) + " " + op.getLabel() + "(" + op.getSourceWord() + "," + op.getTargetWord() + ");");
				
			}				
		}
		// set class wide
		indexShiftBackupAlignment = (double)idx/(double)idx_counter;
		return program;
	}
	
	/***
	 * @param v1 verse one or sentence one
	 * @param v1l lemmatized verse one
	 * @param v2 verse two or sentence two
	 * @param v2l lemmatized verse two
	 * @return List of program lines, each for transforming one word of verse one into a good candidate word from verse two with lowest costs possible.
	 */
	/*public LinkedList<String> transformBasic(LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, LinkedList<String> v1pos, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, LinkedList<String> v2pos, 
			String key){
		
		/////////System.out.println("About to transform verse: " + key);
		LinkedList<String> program = new LinkedList<String>();
		HashSet<Integer> taken = new HashSet<Integer>();

		int i = 0;
		for (String w1 : v1){

			LinkedList<LinkedList<Operation>> all_op_candidates = new LinkedList<LinkedList<Operation>>();
			int j = 0;
			for (String w2 : v2){
				// TODO: this possible needs to be modified in order to suit multiple operation (tree)s?  
				///// ignore? LinkedList<Operation> cheap_ops = this.getMinOp(word_op_trees);	String line = ""; for (Operation o : cheap_ops){line = line + "\t" + o.getLabel();} tmp_program_line = line;
				all_op_candidates.add(this.localTransformation(w1, v1l.get(i), v1n.get(i), v1pos.get(i), w2, v2l.get(j), v2n.get(j), v2pos.get(j), i, j));		
				j++;
			}	
			
			// find the best word couple by looking at the scores ...
			int current_score = 100;
			int used_index = -1;
			int index = 0;
			boolean ignore_blacklisting = false;
			for (LinkedList<Operation> ll_op : all_op_candidates){
				int new_score = 0;
				for (Operation op : ll_op){
					new_score += op.getScore(); // at some point morph and pos operations are introduced, but do not get a score?!
				}
				if ( (new_score < current_score) && !taken.contains(index) ){	
					ignore_blacklisting = false; 
					if ( new_score == 99)
						ignore_blacklisting = true;
					current_score = new_score;
					used_index = index;
				}
				index++;	
			}
			
			// ...and blacklist the winner that is coupled with a word
			String code_line = new String();
			if (used_index >= 0){ // a negative index implies that there is no word matching any relevant score (<100), so fall back OP is needed
				if (!ignore_blacklisting)
					taken.add(used_index);
				Operation final_op = all_op_candidates.get(used_index).getFirst();
				code_line = i + ":" + used_index + " " +final_op.getLabel() + "(" + w1 + "," + final_op.getTargetWord() + ");";
			}
			// this is happening when the 1st verse is longer, because then the index is not updated because even the indexed 0 word of verse 2 is already black-listed (taken). I.e., because the 1st verse is too long and 2nd verse's words are run out
			else{
				code_line = i + " " + "fallback_operation("+w1+");";
			}
			program.add(code_line);
			i++;
		}
		// print the remaining words from verse2 that are not back-listed to endure not to loose too many words on multi-hop approach
		int k = 0;
		for (String w2 : v2){
			if ( !taken.contains(k) )
				program.add(k + " " + "overhang("+w2+");");
			k++;
		}
		return program;
	}*/
		
	/***
	 * This returns several operations sets containing one or more (nested) operations each
	 * @param s1
	 * @param s2
	 * @return Three lists of operations are thinkable: lexical, POS, case; for now, only the first one.
	 */
	public LinkedList<Operation> localTransformation(String s1, String l1, String n1, String p1, String s2, String l2, String n2, String p2, int ii, int jj){
			
		LinkedList<Operation> ops = new LinkedList<Operation>();
		Operation lex_o = new Operation("fallback", 99, s1, s2, ii, jj);
	
		// ################# 1st order operations ###############
		if( s1.equals(s2) ){
			if (p1.equals(p2) || !ControlPanel.pos){
				lex_o = new Operation("NOP", 0, s1, s2, ii, jj);
			}else{
				lex_o = new Operation("NOP_poschange", 1, s1, s2, ii, jj);
			}
		}
		// Got rif of uppering; now only lower casing as means for unifying is considered
		else if ( s1.toLowerCase().equals(s2.toLowerCase()) ){
			if (p1.equals(p2) || !ControlPanel.pos){
				lex_o = new Operation("lower", 1, s1, s2, ii, jj);
			}else{
				lex_o = new Operation("lower_poschange", 2, s1, s2, ii, jj);
			}
		}
		// ################# 2nd order operations ###############
		else if ( n1.equals(n2) && ControlPanel.normalization ){
			if (p1.equals(p2) || !ControlPanel.pos){
				lex_o = new Operation("norm", 2, s1, s2, ii, jj);
			}else{
				lex_o = new Operation("norm_poschange", 3, s1, s2, ii, jj);
			}
		}
		// ################# 2nd order operations ###############
		else if ( l1.equals(l2) ){
			if (p1.equals(p2) || !ControlPanel.pos){
				lex_o = new Operation("lem", 2, s1, s2, ii, jj);
			}else{
				lex_o = new Operation("lem_poschange", 3, s1, s2, ii, jj);
			}
		}
		else if ( ControlPanel.derivationEnabled && Lexicon.areDerivations(l1, l2) ){
			// POS info makes sense here
			if (p1.equals(p2) || !ControlPanel.pos){
				lex_o = new Operation("derivation", 3, s1, s2, ii, jj);
			}else{
				lex_o = new Operation("derivation_poschange", 4, s1, s2, ii, jj);
			}
		}
		// cache comes in here
		else{
			String chached_label = OperateText.getCachedOperation(s1 + "\t" + s2);
			if (chached_label == null){
				// first, check edit distances, then try to match by using 3rd order operations
				if ( s1.length() >= 6 && s2.length() >= 6 && levenshtein(s1, s2) <= 0.28 ){            //  1/6 works;  2/6 does not ; now: 2/7
					// POS info makes sense here
					if (p1.equals(p2) || !ControlPanel.pos){
						lex_o = new Operation("low_editdist", 3, s1, s2, ii, jj);
					}else{
						lex_o = new Operation("low_editdist_poschange", 4, s1, s2, ii, jj);
					}
				}
				// ################# 3rd order operations ###############
				else{
					Operation new_op = thirdOrderLookup(s1, l1, p1, s2, l2, p2, ii, jj);
					// only of 3rd order works, replace default fallback
					if ( new_op != null )
						lex_o = new_op;
				}	
			}else{
				lex_o = new Operation(chached_label, op_costs.get(chached_label), s1, s2, ii, jj);
			}
			//OperateText.cache(s1 + "\t" + s2, lex_o.getLabel()); // cache these ops from low_edit onwards
		} 
		ops.add(lex_o);
		return ops;	
		
	}
	
	// ########## 3rd order (semantic) operations ###############
	public Operation thirdOrderLookup(String s1, String lem1, String pos1, String s2, String lem2, String pos2, int iii, int jjj){	
		
		Operation third_operation = null;
		if ( Lexicon.areSyns(lem1, lem2) ){
			if (pos1.equals(pos2) || !ControlPanel.pos){
				third_operation = new Operation("syn", 4, s1, s2, iii, jjj);
			}else{
				third_operation = new Operation("syn_poschange", 5, s1, s2, iii, jjj);
			}
			return third_operation; //+check_morph(p1, p2);		
		}
		// reuse is hyperonym of bib word
		if ( Lexicon.hyperOf(lem1, lem2) ){
			if (pos1.equals(pos2) || !ControlPanel.pos){
				third_operation = new Operation("hyper", 5, s1, s2, iii, jjj);
			}else{
				third_operation = new Operation("hyper_poschange", 6, s1, s2, iii, jjj);
			}
			return third_operation; //+check_morph(p1, p2);	
		}
		// reuse is hyponym of bib word
		if ( Lexicon.hypoOf(lem1, lem2) ){
			if (pos1.equals(pos2) || !ControlPanel.pos){
				third_operation = new Operation("hypo", 5, s1, s2, iii, jjj);
			}else{
				third_operation = new Operation("hypo_poschange", 6, s1, s2, iii, jjj);
			}
			return third_operation; //+check_morph(p1, p2);	
		}
		// they are co-hyponyms
		if ( ControlPanel.cohyponymsEnabled && Lexicon.areCohypos(lem1, lem2) ){
			if (pos1.equals(pos2) || !ControlPanel.pos){
				third_operation = new Operation("co-hypo", 6, s1, s2, iii, jjj);
			}else{
				third_operation = new Operation("co-hypo_poschange", 7, s1, s2, iii, jjj);
			}		
			return  third_operation; //+check_morph(p1, p2);
		}
		return third_operation;		
	}
	
	
	
	/***
	 * @param v1 verse one or sentence one
	 * @param v1l lemmatized verse one
	 * @param v2 verse two or sentence two
	 * @param v2l lemmatized verse two
	 * @return List of program lines, each for transforming one word of verse one into a good candidate word from verse two with lowest costs possible.
	 */
	public ArrayList<Object> transformParaphrase(
			LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, LinkedList<String> v1pos, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, LinkedList<String> v2pos, 
			String key) {
		op_costs.put("low_editdist", 3); op_costs.put("low_editdist_poschange", 3);
		op_costs.put("syn", 4); op_costs.put("hyper", 5); op_costs.put("hypo", 5); op_costs.put("co-hypo", 6); op_costs.put("fallback", 99); 
		op_costs.put("syn_poschange", 5); op_costs.put("hyper_poschange", 6); op_costs.put("hypo_poschange", 6); op_costs.put("co-hypo_poschange", 7);  // important for caching
		
		ArrayList<Object> programAndInput = new ArrayList<Object>();
		// call this one with the original strings
		programAndInput = Lexicon.findParaphrases(v1, v1l, v1n, v1pos, v2, v2l, v2n, v2pos);
		
		return programAndInput;
	}
	
	
	
	/**
	 * @return A no. between 0 and 1 as the ratio between LD and the longer word's string length.
	 */
	public static double levenshtein(String s, String t){
		int longer = s.length();
		if ( t.length() > s.length() )
			longer = t.length();
		// for all i and j, d[i,j] will hold the Levenshtein distance between the first i characters of s and the first j characters of t;
		Integer[][] d = new Integer[s.length()+1][t.length()+1];
		for ( int i = 0; i <= s.length(); i++ ) {
			for ( int j = 0; j <= t.length(); j++ )
				d[i][j] = 0;
		}		 
		// source prefixes can be transformed into empty string by dropping all characters
		for ( int i = 1; i <= s.length(); i++ )
			d[i][0] = i;
			 
		// target prefixes can be reached from empty source prefix by inserting every character
		for ( int j = 1; j <= t.length(); j++ )
			d[0][j] = j;
		
		int substitutionCost = 0;
		for ( int j = 0; j < t.length(); j++ ){
			for ( int i = 0; i < s.length(); i++ ){
				if ( s.charAt(i) == t.charAt(j) )
			    	substitutionCost = 0;
			    else{
			    	substitutionCost = 1;
			    }
				//			     		deletion		    		insertion	            	substitution
			    d[i+1][j+1] = Math.min( d[i-1+1][j+1] + 1, Math.min(d[i+1][j-1+1] + 1,  d[i-1+1][j-1+1] + substitutionCost));
			}
		} 
		return (double)d[s.length()][t.length()] / ((double)longer) ;
	}
	
	public LinkedList<Object> getFeatures(HashMap<String, Operation> left_ix_op, LinkedList<Object> paraphrcode, LinkedList<Object> extra){
		
		HashMap<String, Integer> features = new HashMap<String, Integer>();
		LinkedList<Object> features_numeric = new LinkedList<Object>();
		for ( String ix : left_ix_op.keySet() ){
			Integer val = features.get(left_ix_op.get(ix).getLabel());
			features.put(left_ix_op.get(ix).getLabel(), val == null ? 1 : val + 1);
		}
		// add the extra OPs from paraphrase code to features as well
		for (Object o : paraphrcode){
			Integer val = features.get( ((Operation) o).getLabel() );
			features.put( ((Operation) o).getLabel() , val == null ? 1 : val + 1);
		}
		
		// add the extra OPs from extra code to features as well
		for (Object o : extra){
			Integer val = features.get( ((Operation) o).getLabel() );
			features.put( ((Operation) o).getLabel() , val == null ? 1 : val + 1);
		}
		double dataset_size = (double) left_ix_op.size() + extra.size();
		
		// create ordered feature set
		features_numeric.add( features.keySet().contains("NOP") ? String.valueOf( Math.round(features.get("NOP")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("NOP_poschange") ? String.valueOf( Math.round(features.get("NOP_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("lower") ? String.valueOf( Math.round(features.get("lower")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("lower_poschange") ? String.valueOf( Math.round(features.get("lower_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("norm") ? String.valueOf( Math.round(features.get("norm")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("norm_poschange") ? String.valueOf( Math.round(features.get("norm_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("lem") ? String.valueOf( Math.round(features.get("lem")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("lem_poschange") ? String.valueOf( Math.round(features.get("lem_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("derivation") ? String.valueOf( Math.round(features.get("derivation")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("derivation_poschange") ? String.valueOf( Math.round(features.get("derivation_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("low_editdist") ? String.valueOf( Math.round(features.get("low_editdist")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("low_editdist_poschange") ? String.valueOf( Math.round(features.get("low_editdist_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("syn") ? String.valueOf( Math.round(features.get("syn")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("syn_poschange") ? String.valueOf( Math.round(features.get("syn_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("hyper") ? String.valueOf( Math.round(features.get("hyper")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("hyper_poschange") ? String.valueOf( Math.round(features.get("hyper_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("hypo") ? String.valueOf( Math.round(features.get("hypo")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("hypo_poschange") ? String.valueOf( Math.round(features.get("hypo_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("co-hypo") ? String.valueOf( Math.round(features.get("co-hypo")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("co-hypo_poschange") ? String.valueOf( Math.round(features.get("co-hypo_poschange")/dataset_size * 1000.0)/1000.0) : "0.0" );
		features_numeric.add( features.keySet().contains("fallback") ? String.valueOf( Math.round(features.get("fallback")/dataset_size * 1000.0)/1000.00) : "0.0" );
		features_numeric.add( features.keySet().contains("phrase") ? String.valueOf( Math.round(features.get("phrase")/dataset_size * 1000.0)/1000.00) : "0.0" );
		return features_numeric;
	}
	
	// TODO: remember these cut indices at some point (in extra data structure)
	public LinkedList<String> cutList(LinkedList<String> ll){
		
		LinkedList<String> ll_new = new LinkedList<String>();
		int counter = 0;
		for (String s : ll){
			if ( !handledIdexesLeft.contains(counter) && !handledIdexesRight.contains(counter)){
				ll_new.add(s);
			}
			counter++;
		}
		return ll_new;
	}
	
	
	/*public LinkedList<Operation> getMinOp(LinkedList<LinkedList<Operation>> op_list){	
		LinkedList<Operation> candidate = new LinkedList<Operation>();
		int tmp_score = 5;
		for (LinkedList<Operation> llo : op_list){
			int tmp_inner_score = 0;
			for ( Operation o : llo ){
				tmp_inner_score = tmp_inner_score + o.getScore();				
			}
			if (tmp_inner_score < tmp_score){
				tmp_score = tmp_inner_score;
				candidate = llo;
			}			
		}
		return candidate;
	}*/
	
}





