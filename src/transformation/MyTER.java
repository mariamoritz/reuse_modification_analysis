package transformation;

import java.util.LinkedList;

import TER.TERtest;

public class MyTER {

	public LinkedList<Object> getVerseScore(
			LinkedList<String> v1, LinkedList<String> v1l, LinkedList<String> v1n, 
			LinkedList<String> v2, LinkedList<String> v2l, LinkedList<String> v2n, String key){	
		
		LinkedList<Object> program = new LinkedList<Object>();
		double terscore;
		terscore = TERtest.TER(listToString(v1), listToString(v2), key);
		//System.out.println(key + " processed");
		program.add(String.valueOf(terscore));
		return program;
	}
	
	public static String listToString(LinkedList<String> list){
		String str = "";
		for (String w : list)
			str = str + w + " ";
		str = str.substring(0, str.length()-1);
		return str;
	}
	
	public static void main(String[] args){
		
		MyTER ter = new MyTER();
		LinkedList<String> v1 = new LinkedList<String>();
		v1.add("the"); v1.add("dog"); v1.add("is"); v1.add("a"); v1.add("nice"); v1.add("cat");
		LinkedList<String> v2 = new LinkedList<String>();
		v2.add("the"); v2.add("dog"); v2.add("is"); v2.add("a"); v2.add("nice"); v2.add("dog");
		
		LinkedList<Object> ll = ter.getVerseScore(v1, v1, v1, v2, v2, v2, "1");
		for (Object o : ll){
			System.out.println((String)o.toString());
		}
	}
}
