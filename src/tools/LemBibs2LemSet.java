package tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Scanner;

public class LemBibs2LemSet {
	
	private HashSet<String> types = new HashSet<String>();
	private static HashSet<String> punctSet = new HashSet<String>();
	
	public void readBiblesFromDirectories(String filepath) throws FileNotFoundException{
		
		File dir = new File(filepath);
		File[] files = dir.listFiles();  
		
		for(File file : files) {
			
			if ( file.getName().endsWith("_lem.txt") ){
				
				Scanner sc = new Scanner(new File(filepath + "/" + file.getName()));
				while (sc.hasNext()) {
					try{
						String[] line = sc.nextLine().split("\\t")[1].split("\\s+");
						for (int i = 0; i < line.length; i++)
							if (!punctSet.contains(line[i]))
								types.add(line[i]);
					}
					catch(ArrayIndexOutOfBoundsException e){
						continue;
					}
				}
				sc.close();
			}
			
		}
	}
	
	
	public static void main(String args[]) throws FileNotFoundException, UnsupportedEncodingException{
		
		// read the lemmatized version of a bible and create a set to query BN/other WNs 
		// to switch to certain Bibles, work with a function instead
		/*Scanner sc1 = new Scanner(new File("data/in/biblestudytools/RHE_lem.txt"));
		Scanner sc2 = new Scanner(new File("data/in/biblestudytools/ASV_lem.txt"));
		Scanner sc3 = new Scanner(new File("data/in/biblestudytools/WBT_lem.txt"));
		Scanner sc4 = new Scanner(new File("data/in/paralleltext/eng-x-bible-kingjames/eng-x-bible-kingjames_lem.txt"));
		Scanner sc5 = new Scanner(new File("data/in/mysword/TCB_lem.txt"));
		Scanner sc6 = new Scanner(new File("data/in/mysword/GEN_lem.txt"));
		Scanner sc61 = new Scanner(new File("data/in/mysword/GREAT_lem.txt"));
		Scanner sc62 = new Scanner(new File("data/in/mysword/MATT_lem.txt"));
		Scanner sc7 = new Scanner(new File("data/in/mysword/DRC_lem.txt"));
		Scanner sc8 = new Scanner(new File("data/in/mysword/ERV_lem.txt"));*/
		PrintWriter pw = new PrintWriter(new File("data/thirdparty/lem_set.txt"), "UTF-8");
		
		punctSet.add(",");punctSet.add(";");punctSet.add(":");punctSet.add("?");punctSet.add("!");punctSet.add(".");punctSet.add("'");
		punctSet.add("--");punctSet.add("`");punctSet.add("[");punctSet.add("]");punctSet.add("(");punctSet.add(")");
		
		LemBibs2LemSet lb2ls = new LemBibs2LemSet();
		lb2ls.readBiblesFromDirectories("data/in/biblestudytools");
		System.out.println(lb2ls.types.size());
		lb2ls.readBiblesFromDirectories("data/in/mysword");
		System.out.println(lb2ls.types.size());
		
		
		for (String word : lb2ls.types)
			pw.println(word);
		
		pw.close();
	}
		

}






