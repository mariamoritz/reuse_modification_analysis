package tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Scanner;

public class BibleTagSetPrinter {
	
	public static void main(String args[]) throws FileNotFoundException, UnsupportedEncodingException{
		
		HashSet<String> hs = new HashSet<String>();
		Scanner sc = new Scanner(new File("data/XXX/deu-x-bible-elberfelder1871_raw_tok_tag.txt"));
		
		while (sc.hasNext()) {
			String line = sc.nextLine();
			String[] lineArray = line.split("\\t");
			hs.add(lineArray[1]);
		}
		sc.close();
		
		for (String tag : hs)
			System.out.println(tag);
	}
}
