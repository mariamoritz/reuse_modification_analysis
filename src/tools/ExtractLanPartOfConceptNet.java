//package tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class ExtractLanPartOfConceptNet {
	
		
	public static void main(String args[]) throws FileNotFoundException, UnsupportedEncodingException{
		
		String lan = "de";
		Scanner sc = new Scanner(new File("conceptnet5.5_assertions_de.csv"));
		PrintWriter pw = new PrintWriter(new File("conceptnet5.5_assertions_de_syn.csv"), "UTF-8");
		PrintWriter pw2 = new PrintWriter(new File("conceptnet5.5_assertions_de_isA.csv"), "UTF-8");
			
		while (sc.hasNext()) {
			String line = sc.nextLine();
			String first_column = line.split("\\t")[0];
			String sec_column = line.split("\\t")[1];
			
			if (first_column.contains("_"))
				continue;
			
			String[] lang = first_column.split(",");
			if ( lang[1].startsWith("/c/" + lan + "/") && lang[2].startsWith("/c/" + lan + "/") ){
				if ( sec_column.equals("/r/Synonym") )
					pw.println(line);	
			
				else if ( sec_column.equals("/r/IsA") )	
					pw2.println(line);
			
			}
				
			
		}
		pw.close();
		pw2.close();
		sc.close();	
		
		
	}
		

}
