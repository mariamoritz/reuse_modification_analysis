package tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class ToolOutput2LemVersion {
	
	public static void main(String args[]) throws FileNotFoundException, UnsupportedEncodingException{
		// MoprhAdorner output handling 
		/*Scanner sc = new Scanner(new File("data/XXX/MATT_orig.ma"));
		PrintWriter pw = new PrintWriter(new File("data/in/mysword/MATT.txt"), "UTF-8");
		
		String verse = "";
		boolean first = true;
		while (sc.hasNext()){
			String[] lineArray = sc.nextLine().split("\\t+");
			try{
				if ( lineArray[2].equals("crd") && ( Integer.parseInt(lineArray[4]) == (int)Integer.parseInt(lineArray[4]) ) && ( (int)Integer.parseInt(lineArray[4]) > 1000 ) ){
					if (!first)
						pw.println(verse);
					else
						first = false;	
					verse = lineArray[0] + "\t"; // switch to 4 for lem bib version (0 for tokenized version)
				}
				else{
					verse = verse + lineArray[0] + " "; // switch to 4 for lem bib version (-"-)
				}
			}
			catch(NumberFormatException e){
					verse = verse + lineArray[0] + " "; // switch to 4 for lem bib version (-"-)
			}		
		}
		pw.println(verse);
		pw.close();
		*/
		
		// handle unknown for Tree-Tagger?
		Scanner sc = new Scanner(new File("data/XXX/eng-x-bible-darby_orig.tt"));
		PrintWriter pw = new PrintWriter(new File("data/in/paralleltext/eng-x-bible-darby/eng-x-bible-darby.txt"), "UTF-8");
		
		String verse = "";
		boolean first = true;
		while (sc.hasNext()){
			String[] lineArray = sc.nextLine().split("\\t+");
			try{
				if ( lineArray[2].equals("@card@") && ( Integer.parseInt(lineArray[0]) == (int)Integer.parseInt(lineArray[0])  )  && ( (int)Integer.parseInt(lineArray[0]) > 1000 ) ){
					if (!first)
						pw.println(verse);
					else
						first = false;	
					verse = lineArray[0] + "\t";					
				}
				else{
					verse = verse + lineArray[0] + " "; // 0->2 for lem
				}
			}
			catch(NumberFormatException e){
					verse = verse + lineArray[0] + " "; // 0->2 for lem
			}		
		}
		sc.close();
		pw.println(verse);
		pw.close();
		
		
		/*// merge lemmatized version of both tools to find inconsitencies:
		Scanner sc1 = new Scanner(new File("data/in/biblestudytools/WBT_lem_ma.txt"));
		Scanner sc2 = new Scanner(new File("data/in/biblestudytools/WBT_lem.txt"));
		LinkedList<String> ll1 = new LinkedList<String>();
		LinkedList<String> ll2 = new LinkedList<String>();
		HashSet<String> hs = new HashSet<String>();
		while ( sc1.hasNext() ) {
			String line = sc1.nextLine();
			ll1.add(line);			
		}
		while ( sc2.hasNext() ) {
			String line = sc2.nextLine();
			ll2.add(line);			
		}
		for (int i = 0; i < ll1.size(); i++){
			String[] tokens = ll1.get(i).split("\\t+")[1].split(" ");
			String[] tokens2 = ll2.get(i).split("\\t+")[1].split(" ");
			try{
				if (tokens.length == tokens2.length){
					for (int j = 0; j < tokens.length; j++){
						if (!tokens[j].equals(tokens2[j]))
							hs.add(tokens[j] + " " + tokens2[j]);
					}
				}
				else{
					System.out.println(ll1.get(i).split("\\t+")[0]);
				}
			}
			catch(IndexOutOfBoundsException e){
				for (int j = 0; j < tokens.length; j++){
					System.out.print(tokens[j] + " ");
				} 
				System.out.println();
				for (int j = 0; j < tokens2.length; j++){
					System.out.print(tokens2[j] + " ");
				} 
				System.out.println();
			
			}
		}
		for (String s : hs)
			System.out.println(s);
		*/
	}

}
