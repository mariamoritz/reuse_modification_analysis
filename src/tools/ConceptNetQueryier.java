package tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class ConceptNetQueryier {
	
	public static void main(String args[]) throws FileNotFoundException, UnsupportedEncodingException{

		
		HashSet<HashSet<String>> synonyms = new HashSet<HashSet<String>>();
		HashSet<LinkedList<HashSet<String>>> hyponyms = new HashSet<LinkedList<HashSet<String>>>();
		HashSet<LinkedList<HashSet<String>>> hypernyms = new HashSet<LinkedList<HashSet<String>>>();
		//HashSet<HashSet<String>> cohyponyms = new HashSet<HashSet<String>>();
		
		HashSet<String> syn_couples = new HashSet<String>();
				
		Scanner sc1 = new Scanner(new File("conceptnet5.5_assertions_en_syn.csv"));
		Scanner sc2 = new Scanner(new File("conceptnet5.5_assertions_en_isA.csv"));
		Scanner sc3 = new Scanner(new File("conceptnet5.5_assertions_en_isA.csv"));
		/*Scanner sc1 = new Scanner(new File("test_en_syn.csv"));
		Scanner sc2 = new Scanner(new File("test_en_isA.csv"));
		Scanner sc3 = new Scanner(new File("test_en_isA.csv"));*/
		
		PrintWriter syns = new PrintWriter(new File("conceptnet5.5_de_syn.txt"), "UTF-8");
		PrintWriter hypers = new PrintWriter(new File("conceptnet5.5_de_hypers.txt"), "UTF-8"); 
		PrintWriter hypos = new PrintWriter(new File("conceptnet5.5_de_hypos.txt"), "UTF-8"); 
		PrintWriter cohypos = new PrintWriter(new File("conceptnet5.5_de_cohypos.txt"), "UTF-8"); 
		
		// handle syns
		int c = 0;
		while (sc1.hasNext()) {
			c++;
			if (c%500 == 0)
				System.out.println(c + " lines read");
			String line = sc1.nextLine();
			//String sec_column = line.split("\\t")[1];
			//if ( sec_column.equals("/r/Synonym") )				pw.println(line);	
			//else if ( sec_column.equals("/r/IsA") )				pw2.println(line);
			String first_column = line.split("\\t")[0];
			
			if (first_column.contains("_"))
				continue;
			
			String[] lang = first_column.split(",");
			if (lang[1].contains("/en/") && lang[2].contains("/en/")){
				// extract words
				String str1 = lang[1].split("/")[3];
				String str2 = lang[2].split("/")[3];
				syn_couples.add(str1+ " " +str2);
					
				// save syns
				boolean notcontained = true;
				for (HashSet<String> hs : synonyms){
					
					if (hs.contains(str1) || hs.contains(str2)){
						hs.add(str1);
						hs.add(str2);
						notcontained = false;
					}					
				}
				if (notcontained){
					HashSet<String> nhs = new HashSet<String>();
					nhs.add(str1); nhs.add(str2); synonyms.add(nhs);					
				}	
			}
		}
		sc1.close();	
		// print syns
		for (HashSet<String> hs : synonyms){
			for (String s : hs)
				syns.print(s + " ");
			syns.println();			
		}
		syns.close();
	
	
		
		//################################################################################
		// hypers etc.
		c = 0;
		while (sc2.hasNext()) {
			c++;
			if (c%500 == 0)
				System.out.println(c + " lines read");
			String line = sc2.nextLine();
			String first_column = line.split("\\t")[0];
			
			if (line.contains("_"))
				continue;
			
			String[] lang = first_column.split(",");
			if (lang[1].contains("/en/") && lang[2].contains("/en/")){
				String str1 = lang[1].split("/")[3];
				String str2 = lang[2].split("/")[3];
				//System.out.println(str1);
					
				// save hypers
				boolean notcontained = true;
				Iterator<LinkedList<HashSet<String>>> it = hypernyms.iterator();
				while (it.hasNext()){
					
					LinkedList<HashSet<String>> ll = it.next();
					HashSet<String> ss = ll.getFirst();
					HashSet<String> hyper = ll.getLast();	
					
					// define word and potential synonym
					String key_ss = str1;
					String key_hypr = str2;
					for (String s : ss){
						key_ss = s;
						break;
					}
					for (String s : hyper){
						key_hypr = s;
						break;
					}
					
					boolean are_syns1 = false; boolean are_syns2 = false; 
					// this basically tests if a new worset-hyperset is necessary to create for the new word-hypernym-couple, because I find never synonymy to words from exist. worset-hyperset pairs  
					if (syn_couples.contains(str1+" "+key_ss) || syn_couples.contains(key_ss+" "+str1)){
						ss.add(str1);
						are_syns1 = true;
						if (are_syns2)
								notcontained = false;
					}
					if (syn_couples.contains(str2+" "+key_hypr) || syn_couples.contains(key_hypr+" "+str2)){
						hyper.add(str2);
						are_syns2 = true;
						if (are_syns1)
							notcontained = false;			
					}
					// expand the ll entry /*if (!notcontained){tmpll.add(ss); tmpll.add(hyper); it.remove(); hypernyms.add(tmpll);	}*/			
				}					
				// create a new entry in the list	
				if (notcontained){
					LinkedList<HashSet<String>> ll = new LinkedList<HashSet<String>>();			
					HashSet<String> nhs = new HashSet<String>(); nhs.add(str1);
					HashSet<String> nhs2 = new HashSet<String>(); nhs2.add(str2);
					ll.add(nhs); ll.add(nhs2);
					hypernyms.add(ll);					
				}				
			}
		}
		sc2.close();	
		// print hypers		
		for (LinkedList<HashSet<String>> ll : hypernyms){		
			HashSet<String> ss = ll.getFirst();
			HashSet<String> hyper = ll.getLast();		
			for (String s : ss)
				hypers.print(s + " ");
			hypers.print("\t");
			for (String s : hyper)
				hypers.print(s + " ");
			hypers.println();	
		}
		hypers.close();
		
		
		
		//################################################################################
		// hypos etc.
		c = 0;
		while (sc3.hasNext()) {
			c++;
			if (c%500 == 0)
				System.out.println(c + " lines read");
			String line = sc3.nextLine();
			String first_column = line.split("\\t")[0];
			
			if (line.contains("_"))
				continue;
			
			String[] lang = first_column.split(",");
			if (lang[1].contains("/en/") && lang[2].contains("/en/")){
				String str1 = lang[1].split("/")[3];
				String str2 = lang[2].split("/")[3];
				//System.out.println(str1);
					
				// save hypos
				boolean notcontained = true;
				Iterator<LinkedList<HashSet<String>>> it = hyponyms.iterator();
				while (it.hasNext()){
					
					LinkedList<HashSet<String>> ll = it.next();
					HashSet<String> ss = ll.getFirst();
					HashSet<String> hypo = ll.getLast();	
					
					// define word and potential synonym
					String key_hypo = str1;
					String key_ss = str2;
					for (String s : ss){
						key_ss = s;
						break;
					}
					for (String s : hypo){
						key_hypo = s;
						break;
					}
					
					boolean are_syns1 = false; boolean are_syns2 = false; 				
					if (syn_couples.contains(str2+" "+key_ss) || syn_couples.contains(key_ss+" "+str2)){
						ss.add(str2);
						are_syns1 = true;
						if (are_syns2)
								notcontained = false;
					}
					if (syn_couples.contains(str1+" "+key_hypo) || syn_couples.contains(key_hypo+" "+str1)){
						hypo.add(str2);
						are_syns2 = true;
						if (are_syns1)
							notcontained = false;			
					}
					
					
						
				}					
				// create a new entry in the list	
				if (notcontained){
					LinkedList<HashSet<String>> ll = new LinkedList<HashSet<String>>();			
					HashSet<String> nhs = new HashSet<String>(); nhs.add(str2);
					HashSet<String> nhs2 = new HashSet<String>(); nhs2.add(str1);
					ll.add(nhs); ll.add(nhs2);
					hyponyms.add(ll);					
				}				
			}
		}
		sc3.close();	
		// print hypos		
		for (LinkedList<HashSet<String>> ll : hyponyms){		
			HashSet<String> ss = ll.getFirst();
			HashSet<String> hypo = ll.getLast();		
			for (String s : ss)
				hypos.print(s + " ");
			hypos.print("\t");
			for (String s : hypo)
				hypos.print(s + " ");
			hypos.println();	
		}
		hypos.close();
		
		

		//################################################################################
		// print co-hypos simply based on the existing hypernym structure		
		// print hypers		
		for (LinkedList<HashSet<String>> ll : hypernyms){		
			HashSet<String> ss = ll.getFirst();		
			for (String s : ss)
				cohypos.print(s + " ");
			cohypos.println();	
		}
		cohypos.close();
	}
	
		
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
