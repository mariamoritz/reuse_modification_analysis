package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashSet;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Scanner;

import edu.cmu.meteor.scorer.MeteorConfiguration;
import edu.cmu.meteor.scorer.MeteorScorer;
import edu.cmu.meteor.util.Constants;
import model.Bible;
import model.Lexicon;
import model.ReadData;
import edu.cmu.meteor.scorer.MeteorConfiguration;
import edu.cmu.meteor.scorer.MeteorScorer;
import edu.cmu.meteor.util.Constants;

public class ControlPanel {
	
	// lexicon is a global variable to generate it only once
	public static int cachesz;
	public static int parallelthreads;
	public static int maxthreads;
	public static boolean remainingalignments;
	public static boolean cohyponymsEnabled;
	public static boolean features;
	public static boolean prealignment;
	public static boolean combinedalignment;
	public static boolean derivationEnabled;
	public static boolean babelnet;
	public static boolean conceptnet;
	public static boolean conceptnetpairs;
	public static boolean paraphrases;
	public static String lang;
	public static Lexicon lex;
	public static boolean normalization;
	public static boolean pos;
	public static boolean idxshift;
	public static boolean PER;
	public static boolean BLEU;
	public static boolean TER;
	public static boolean METEOR;
	public static MeteorScorer scorer;
	
	
	public static void main(String args[]) throws IOException{
		
		//long start = System.currentTimeMillis();	
		// load properties		
		InputStream input = new FileInputStream(args[0]);
		Properties prop = new Properties();
		prop.load(input);
		
		cachesz = Integer.valueOf(prop.getProperty("cachesz"));
		parallelthreads = Integer.valueOf(prop.getProperty("parallelthreads"));
		maxthreads = Integer.valueOf(prop.getProperty("maxthreads"));
		remainingalignments = Boolean.valueOf(prop.getProperty("remainingalignments"));
		cohyponymsEnabled = Boolean.valueOf(prop.getProperty("cohyponyms"));
		features = Boolean.valueOf(prop.getProperty("features"));
		prealignment = Boolean.valueOf(prop.getProperty("prealignment"));
		combinedalignment = Boolean.valueOf(prop.getProperty("combinedalignment"));
		derivationEnabled = Boolean.valueOf(prop.getProperty("derivation"));
		lang = String.valueOf(prop.getProperty("lang"));
		babelnet = Boolean.valueOf(prop.getProperty("babelnet"));
		conceptnet = Boolean.valueOf(prop.getProperty("conceptnet"));
		conceptnetpairs = Boolean.valueOf(prop.getProperty("conceptnetpairs"));
		paraphrases = Boolean.valueOf(prop.getProperty("paraphrases"));
		lex = new Lexicon(babelnet, conceptnet, conceptnetpairs, lang, cohyponymsEnabled, derivationEnabled, paraphrases);
		normalization = Boolean.valueOf(prop.getProperty("normalization"));
		pos = Boolean.valueOf(prop.getProperty("pos"));
		idxshift = Boolean.valueOf(prop.getProperty("idxshift"));
		PER = Boolean.valueOf(prop.getProperty("PER"));
		BLEU = Boolean.valueOf(prop.getProperty("BLEU"));
		TER = Boolean.valueOf(prop.getProperty("TER"));
		METEOR = Boolean.valueOf(prop.getProperty("METEOR"));
		
		if (METEOR){
			MeteorConfiguration config = new MeteorConfiguration();
			config.setLanguage("en");
			config.setNormalization(Constants.NORMALIZE_KEEP_PUNCT);
			scorer = new MeteorScorer(config);
		}
		
		ReadData rd = new ReadData("development", args[1]); // reads the data from the datafile
		LinkedList<Bible> list = rd.getDataSet();	
		LinkedList<Bible> listLem = rd.getDataSetLem();	
		LinkedList<Bible> listNorm = rd.getDataSetNorm();
		LinkedList<Bible> listPOS = rd.getDataSetPOS();
		//OperateText ot = new OperateText(parallelthreads, cachesz);
		//ot.processText(list.get(0), listLem.get(0), list.get(1), listLem.get(1), args[0]);
		
		// run only processes for two files where an alignment file exists
		int thread = 0;
		String saved_alignment = "";
		LinkedList<String> saved_alignments = new LinkedList<String>();
		HashSet<String> active_alignments = new HashSet<String>();
		PrintWriter alignemnts_pw = new PrintWriter(new File("alignments.out"), "UTF-8");//out comment for threads
		File folder = new File("data" + File.separator +"in" + File.separator + "alignments"); // alignment files that exist 
		// remaining alignments from a multi-thread
		if (remainingalignments){
			Scanner sc = new Scanner(new File("alignments.in"));
			while(sc.hasNext())
				active_alignments.add(sc.nextLine());
			sc.close();
		}
		
		for (int i = 0; i < list.size(); i++){
			for (int j = 0; j < list.size(); j++){	
				boolean alignment_exists = false;
				// iterate over the alignment files    
				for (File file : folder.listFiles()) {
				    if ( file.getName().startsWith("training.align." + list.get(i).getLabel() + "_" + list.get(j).getLabel()) ){
				    	alignment_exists = true;
				    	saved_alignment = "training.align." + list.get(i).getLabel() + "_" + list.get(j).getLabel();
				    	break;
				    }
				}
				if (alignment_exists){
					OperateText ot = new OperateText(parallelthreads, cachesz);
					// alignments from file
					if (remainingalignments) {
						if ( active_alignments.contains(saved_alignment) ){
							ot.processText(list.get(i), listLem.get(i), listNorm.get(i), listPOS.get(i), list.get(j), listLem.get(j), listNorm.get(j), listPOS.get(j));
							thread++;
						}
						else{
							System.out.println("##################Don't Process!################");
						}
					}
					// standard
					else{
						// save remaining alignments
						if (thread >= maxthreads){
							saved_alignments.add(saved_alignment);
							alignemnts_pw.println(saved_alignment);
						}
						else{
							ot.processText(list.get(i), listLem.get(i), listNorm.get(i), listPOS.get(i), list.get(j), listLem.get(j), listNorm.get(j), listPOS.get(j));
							thread++;
						}
					}	
				}	
			}
		}
		alignemnts_pw.close();
		
		//System.out.println(printTime(start));
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		//Date date = new Date();
		//System.out.println("Programm started at: " + dateFormat.format(date)); //2016/11/16 12:08:43
	}

	public static String printTime(long startTime){		
		long currentTime = System.currentTimeMillis();
		long diff = currentTime - startTime;
		double t = Double.valueOf(diff)/(1000.0*60.0);
		return "Program took " + t + " minutes.";		
	}
	
}
