package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

import transformation.Transformation;
import transformation.PER;
import transformation.BLEU;
import transformation.MyTER;
import transformation.MyMETEOR;
import model.Bible;
import controller.ControlPanel;

public class OperateText {
	
	private static boolean cache_full = false;
	private int parallelthreads = 0;
	private static int cachesz = 0;
	public static HashMap<String, String> cache = new HashMap<String, String>();
	
	public OperateText(int parallelthreads, int cachesz){
		this.parallelthreads = parallelthreads;
		OperateText.cachesz = cachesz;
	}
  
	public static String getCachedOperation(String wordpair){
		return cache.get(wordpair);
	}
	
	public static void cache(String wordpair, String opLabel){
		if (cache.size() < cachesz){
			cache.put(wordpair, opLabel);
		}
		else if (!cache_full){
			cache_full = true;
			System.out.println("################# Cache full! This is slowing down the program! ###################");
		}
	}
	
	public void processText(Bible bib_one, Bible bib_lem_one, Bible bib_norm_one, Bible bib_pos_one, Bible bib_two, Bible bib_lem_two, Bible bib_norm_two, Bible bib_pos_two) throws FileNotFoundException, UnsupportedEncodingException{

		// Ensure that two Bibles share the same verses
		LinkedList<Bible> three_bibles = getIntersections(bib_one, bib_two, true);
		final Bible one = three_bibles.getFirst();
		final Bible two = three_bibles.get(1);
		final Bible alignment = three_bibles.getLast();	
		System.out.println("Intersection of Bibles (incl. pre-aligned data) done.");
		
		// Ensure that lemmatized Bibles share the same verses
		LinkedList<Bible> two_bibles_lem = getIntersections(bib_lem_one, bib_lem_two);
		final Bible oneL = two_bibles_lem.getFirst();
		final Bible twoL = two_bibles_lem.getLast();
		System.out.println("Intersection of Lem Bibles done.");
		
		// Ensure that normalized Bibles share the same verses
		LinkedList<Bible> two_bibles_norm = getIntersections(bib_norm_one, bib_norm_two);
		final Bible oneN = two_bibles_norm.getFirst();
		final Bible twoN = two_bibles_norm.getLast();
		System.out.println("Intersection of Norm Bibles done.");
		
		// Ensure that normalized Bibles share the same verses
		LinkedList<Bible> two_bibles_pos = getIntersections(bib_pos_one, bib_pos_two);
		final Bible onePOS = two_bibles_pos.getFirst();
		final Bible twoPOS = two_bibles_pos.getLast();
		System.out.println("Intersection of POS Bibles done.");
				
		// ########### prepare data chunks for all Bibles (based on the key sets) for MULTITHREADING ############
		// list of key sets
		ArrayList<HashSet<String>> ChunkerData = new ArrayList<HashSet<String>>();
		int datasize = one.getPlainOrdered().keySet().size();
		int chunk_size = (int)Math.ceil(new Double((double)datasize/(double)this.parallelthreads).intValue());
		int cnt = 1;
		HashSet<String> hs = new HashSet<String>();  // temporarily refilled key set
		for (String key : one.getPlainOrdered().keySet()){
			// save 1/10 of the data's keys in the new hash set to the overall maps
			hs.add(key);
            if (cnt % chunk_size == 0) {
                //System.out.println("make new hm with counter being" + cnt + " key: " + k);
                ChunkerData.add(hs);
                hs = new HashSet<String>(); 
            }
            cnt++;
		}
		ChunkerData.add(hs);
		
		//PrintWriter pw = new PrintWriter(new File( "log_" + bib_one.getLabel() + "_" + bib_two.getLabel() + ".out"), "UTF-8");//out comment for threads
		// BEGIN RUNNING THREADS ###################################
		// run over the data set
		int external_file_counter = 0;
		for ( HashSet<String> hashs : ChunkerData ){	
			// get the current key set
			final HashSet<String> hset = hashs;
			final int file_counter = external_file_counter;
			final String process_label = bib_one.getLabel() + "_" + bib_two.getLabel();
			Thread t = new Thread(new Runnable() {
				public void run(){
			// uncomment for threads
			PrintWriter pw = null;
					//double fisrt_id = (double)hashma.size()/(double)t_counter;
			try {
				pw = new PrintWriter(new File( "log_" + process_label + "_" + file_counter + ".out"), "UTF-8");
				System.out.println("Print writer created.");
			} catch (Exception e1) {
				e1.printStackTrace();
			}	
					//####################################
			int counter = 0;
			for (String ky : hset){
				counter++;
				LinkedList<String> verse1 = one.getPlainOrdered().get(ky);
				LinkedList<String> verse1lem = oneL.getPlainOrdered().get(ky);
				LinkedList<String> verse1norm = oneN.getPlainOrdered().get(ky);
				LinkedList<String> verse1pos = onePOS.getPlainOrdered().get(ky);
				LinkedList<String> verse2 = two.getPlainOrdered().get(ky);
				LinkedList<String> verse2lem = twoL.getPlainOrdered().get(ky);
				LinkedList<String> verse2norm = twoN.getPlainOrdered().get(ky);
				LinkedList<String> verse2pos = twoPOS.getPlainOrdered().get(ky);
				LinkedList<String> verseAligned = alignment.getPlainOrdered().get(ky);
							
						// ##############  call the compare function here ################
						// OUTPUT: is a list of operations, is a matrix when >>features<< is true
						// OUTPUT: contains co-hyponym, normalization, derivation, POS change, idxshift OPs (the last OP) when corresponding configuration is set to true  
						// ORDER: NOP CASEFOLDING [NORM] LEM LOWEDITDIST[POSCHANGE] [DERIVATION[POSCHANGE]] SYN HYPER HYPO [CO-HYPO] FALLBACK IDXSHIFT
				LinkedList<Object> code = new LinkedList<Object>();
				if (ControlPanel.PER){
					PER per = new PER();
					code = per.getVerseScore(verse1, verse1lem, verse1norm, verse2, verse2lem, verse2norm, ky);
					String print_line = ky + "\t";
					if (code != null){
						for (Object line : code)
							print_line = print_line + line.toString() + "\t";
						pw.println(print_line + alignment.getPlainOrdered().size() + "\t" + alignment.getYears());
						//break;
						if (file_counter == 0)
							System.out.println("Verse no. " + ky + " done. Processed about " + String.valueOf(counter*parallelthreads) + " verses.");
					}
						
				}
				else if (ControlPanel.BLEU){
					BLEU bleu = new BLEU();
					code = bleu.getVerseScore(verse1, verse1lem, verse1norm, verse2, verse2lem, verse2norm, ky);
					String print_line = ky + "\t";
					if (code != null){
						for (Object line : code)
							print_line = print_line + line.toString() + "\t";
						pw.println(print_line + alignment.getPlainOrdered().size() + "\t" + alignment.getYears());
						//break;
						if (file_counter == 0)
							System.out.println("Verse no. " + ky + " done. Processed about " + String.valueOf(counter*parallelthreads) + " verses.");
					}
					
				}
				else if (ControlPanel.TER){
					MyTER ter = new MyTER();
					code = ter.getVerseScore(verse1, verse1lem, verse1norm, verse2, verse2lem, verse2norm, ky);
					String print_line = ky + "\t";
					if (code != null){
						for (Object line : code)
							print_line = print_line + line.toString() + "\t";
						pw.println(print_line + alignment.getPlainOrdered().size() + "\t" + alignment.getYears());
						//break;
						if (file_counter == 0)
							System.out.println("Verse no. " + ky + " done. Processed about " + String.valueOf(counter*parallelthreads) + " verses.");
					}
						
				}
				else if (ControlPanel.METEOR){
					MyMETEOR meteor = new MyMETEOR();
					code = meteor.getVerseScore(verse1, verse1lem, verse1norm, verse2, verse2lem, verse2norm, ky, ControlPanel.scorer);
					String print_line = ky + "\t";
					if (code != null){
						for (Object line : code)
							print_line = print_line + line.toString() + "\t";
						pw.println(print_line + alignment.getPlainOrdered().size() + "\t" + alignment.getYears());
						//break;
						if (file_counter == 0)
							System.out.println("Verse no. " + ky + " done. Processed about " + String.valueOf(counter*parallelthreads) + " verses.");
					}
						
				}
				else if (ControlPanel.prealignment){
					Transformation t = new Transformation();
					code = t.transformBerkeley(verse1, verse1lem, verse1norm, verse1pos, verse2, verse2lem, verse2norm, verse2pos, verseAligned, ky);
					String print_line = ky + "\t";
					if (code != null){
						for (Object line : code)
							print_line = print_line + line.toString() + "\t";
						pw.println(print_line + alignment.getPlainOrdered().size() + "\t" + alignment.getYears());
						//break;
						if (file_counter == 0)
							System.out.println("Verse no. " + ky + " done. Processed about " + String.valueOf(counter*parallelthreads) + " verses.");
					}
				}
				else{
					Transformation t = new Transformation();
					code = t.transform(verse1, verse1lem, verse1norm, verse1pos, verse2, verse2lem, verse2norm, verse2pos, ky);		
					String print_line = ky + "\t";
					if (code != null){
						for (Object line : code)
							print_line = print_line + line.toString() + "\t";
						pw.println(print_line + one.getPlainOrdered().size() + "\t" + one.getYears());
						//break;
						if (file_counter == 0)
							System.out.println("Verse no. " + ky + " done. Processed about " + String.valueOf(counter*parallelthreads) + " verses.");
					}
				}
			}
			pw.close();	
		}
			});
			t.start();
		external_file_counter++;
		}
		// EBND RUNNING THREADS	###################################
			
		// Uncomment this and get the un-threaded version (OUTDATED) ##########################		
		// get the aligned verses of two Bibles in normal format and lemmatized version
		/*PrintWriter pw = new PrintWriter(new File( bib_one.getLabel() + "-" + bib_two.getLabel() + ".out"), "UTF-8");	
		for (String key1 : one.getPlainOrdered().keySet()){
			LinkedList<String> verse1 = one.getPlainOrdered().get(key1);
			LinkedList<String> verse1lem = oneL.getPlainOrdered().get(key1);
			LinkedList<String> verse2 = two.getPlainOrdered().get(key1);
			LinkedList<String> verse2lem = twoL.getPlainOrdered().get(key1);
				
			// ##############  call the compare function here ################
			Transformation t = new Transformation();
			LinkedList<String> code = t.transform(verse1, verse1lem, verse2, verse2lem, key1);
			String print_line = key1 + "\t";	
			for (String line : code)
				print_line = print_line + line + "\t";
			pw.println(print_line);
			//break;			
		}
		pw.close();*/
		
	}
	
	// ########################################################################################
	
	// update the Bible content to  make sure that only verses in the intersection are considered
	public LinkedList<Bible> getIntersections(Bible one, Bible two){
		
		LinkedList<Bible> ll = new LinkedList<Bible>();
		Bible new_one = new Bible();
		Bible new_two = new Bible();
		HashMap<String, LinkedList<String>> hash_new_one = new HashMap<String, LinkedList<String>>();
		HashMap<String, LinkedList<String>> hash_new_two = new HashMap<String, LinkedList<String>>();
		HashMap<String, Integer> hm =  new HashMap<String, Integer>();
		for (String key : one.getPlainOrdered().keySet()){
			hm.put(key, 1);
		}
		// increase the bible verse hit rate when both bibles contain this verse
		for (String key : two.getPlainOrdered().keySet()){
			if (hm.containsKey(key))
				hm.put(key, 2);
		}
		
		// only if the rate is 2 take this linkedList and the relating key over into the new hash map		
		for (String key : one.getPlainOrdered().keySet()){
			if (hm.get(key) > 1){
				LinkedList<String> tmp = one.getPlainOrdered().get(key);
				hash_new_one.put(key, tmp);
			}
		}
		for (String key : two.getPlainOrdered().keySet()){
			try{ // work around null pointer ex if the requested verse id is not even in the hashmap hm
				if (hm.get(key) > 1){
					LinkedList<String> tmp = two.getPlainOrdered().get(key);
					hash_new_two.put(key, tmp);
				}
			}
			catch(NullPointerException e){
				continue;
			}
		}
		new_one.setPlainOrdered(hash_new_one);
		new_one.setLabel(one.getLabel());
		new_two.setPlainOrdered(hash_new_two);
		new_two.setLabel(two.getLabel());
		
		ll.add(new_one);
		ll.add(new_two);	
		return ll;
	}

	// update the Bible content to  make sure that only verses in the intersection are considered
	public LinkedList<Bible> getIntersections(Bible one, Bible two, Boolean alignment) throws FileNotFoundException{
			
		LinkedList<Bible> biblelist = new LinkedList<Bible>();
		Bible new_one = new Bible();
		Bible new_two = new Bible();
		Bible new_aligned = new Bible();
		HashMap<String, LinkedList<String>> hash_new_one = new HashMap<String, LinkedList<String>>();
		HashMap<String, LinkedList<String>> hash_new_two = new HashMap<String, LinkedList<String>>();
		HashMap<String, LinkedList<String>> hash_new_aligned = new HashMap<String, LinkedList<String>>();
		HashMap<String, Integer> hm =  new HashMap<String, Integer>();
		for (String key : one.getPlainOrdered().keySet()){
			if (one.getPlainOrdered().get(key).size() > 0)
				hm.put(key, 1);
		}
		// increase the bible verse hit rate when both bibles contain this verse
		for (String key : two.getPlainOrdered().keySet()){
			if (hm.containsKey(key) && two.getPlainOrdered().get(key).size() > 0)
				hm.put(key, 2);
		}
		
		// only if the rate is 2 take this linkedList and the relating key over into the new hash map		
		for (String key : one.getPlainOrdered().keySet()){
			if (hm.get(key) > 1){
				LinkedList<String> tmp = one.getPlainOrdered().get(key);
				hash_new_one.put(key, tmp);
			}
		}
		for (String key : two.getPlainOrdered().keySet()){
			try{ // work around null pointer ex if the requested verse id is not even in the hashmap hm
				if (hm.get(key) > 1){
					LinkedList<String> tmp = two.getPlainOrdered().get(key);
					hash_new_two.put(key, tmp);
				}
			}
			catch(NullPointerException e){
				continue;
			}
		}
		
		// create the alignment Bible here if the configuration says it, if not, this block is ignored and only two Bibles are returned
		if (ControlPanel.prealignment){
			String align_file = "";
			File folder = new File("data" + File.separator +"in" + File.separator + "alignments");
		    for (File file : folder.listFiles()) {
		    	if ( file.getName().startsWith("training.align." + one.getLabel() + "_" + two.getLabel()) ){
		    		align_file = "data" + File.separator +"in" + File.separator + "alignments" + File.separator + file.getName();	
		    		break;
		    	}
		    }
			Scanner sc = new Scanner(new File(align_file));
			while (sc.hasNext()){
				LinkedList<String> ll = new LinkedList<>();
				String l = sc.nextLine();
				String[] line = l.split("\\t+");
				String id = line[0].trim();
				if (id.length() < 8) id = "0" + id; 
				try{// see above
					// work around null pointer ex (in next line) if the requested verse id is not even in the hashmap hm
					if (hm.get(id) > 1){
						try{
							for (int i = 0; i < line[1].split(" ").length; i++){
								ll.add(line[1].split(" ")[i]);
								//System.out.print(line[1].split(" ")[i] + " ");
								//System.out.println();
							}
							hash_new_aligned.put(id, ll);
						}
						// for what exactly is this?
						catch(IndexOutOfBoundsException e){
							continue;
						}
					}
				}
				catch(NullPointerException e2){
					System.out.println("Problems with Bib: " + one.getLabel() + " and " + two.getLabel() + ", verse " + id);
					continue;
				}
			}
			sc.close();
			new_aligned.setPlainOrdered(hash_new_aligned);
			new_aligned.setLabel(one.getLabel() + "_" + two.getLabel());
			new_aligned.setYears(align_file.split("_")[2]);
		}
			
		new_one.setPlainOrdered(hash_new_one);
		new_one.setLabel(one.getLabel());
		new_two.setPlainOrdered(hash_new_two);
		new_two.setLabel(two.getLabel());	
		biblelist.add(new_one);
		biblelist.add(new_two);	
		
		if (ControlPanel.prealignment)
			biblelist.add(new_aligned);
		
		return biblelist;
	}
	
}














