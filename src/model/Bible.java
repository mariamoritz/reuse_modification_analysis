package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

public class Bible {

	// necessary for inverted data handling only
	private HashSet<String> punctSet = new HashSet<String>();
	private LinkedList<String> versenames = new LinkedList<String>();
	private LinkedList<String> wordformes = new LinkedList<String>();
	private HashMap<String, String> invertedList = new HashMap<String, String>();
	private HashMap<String, HashSet<String>> plainText = new HashMap<String, HashSet<String>>();
	
	private HashMap<String, LinkedList<String>> plainTextOrdered = new HashMap<String, LinkedList<String>>();
	private String label = new String();
	private String years = new String();
	private String year = new String();
	
	public Bible(String filename, boolean annotated){}
	
	public Bible(){}
	
	// read one Bible from flat file
	public Bible(String filename) throws FileNotFoundException{
		// punctuation to filter
		punctSet.add(",");punctSet.add(";");punctSet.add(":");punctSet.add("?");punctSet.add("!");punctSet.add(".");punctSet.add("'");punctSet.add("--");punctSet.add("`");punctSet.add("[");punctSet.add("]");punctSet.add("(");punctSet.add(")");
		Scanner sc = new Scanner(new File(filename));
		
		while (sc.hasNext()){
			LinkedList<String> ll = new LinkedList<String>();
			String line = sc.nextLine();
			if (line.startsWith("#"))
				continue;
			String key = line.split("\\t")[0].trim();
			if (key.length() < 8)
				key = "0" + key; 
			try{
				String[] verse = line.split("\\t")[1].split(" ");
				for (String w : verse){
					if (!punctSet.contains(w))
						ll.add(w);
				}
				this.plainTextOrdered.put(key, ll);	
			}
			catch(ArrayIndexOutOfBoundsException e){
				continue;
			}
		}
		sc.close();	
	}
		
	// this is necessary for the intersection of the verses, later in the controller
	public void setPlainOrdered(HashMap<String, LinkedList<String>> hm){		
		this.plainTextOrdered = hm;
	}
	
	// get plain default Bible
	public HashMap<String, LinkedList<String>> getPlainOrdered(){		
		return this.plainTextOrdered;
	}
	
	public void setLabel(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public void setYears(String years){
		this.years = years;
	}
	
	public String getYears(){
		return this.years;
	}
	
	public void setYear(String year){
		this.year = year;
	}
	
	public String getYear(){
		return this.year;
	}
	
	// ########################################################################################
	
	
	// read a single bible directory from inverse list
	public Bible(File directory) throws FileNotFoundException{

		File[] listOfFiles = directory.listFiles();
		String filename = "";
		
		for (File file : listOfFiles) {
			if (file.isFile()) {
				
				String name = file.getName();
				if (name.contains(".txt") && !name.equals("versenames.txt"))
					filename = name;
				// save word forms in array
				if (name.endsWith(".wordforms")){
					Scanner sc = new Scanner(file);
					int c1 = 0;
					while (sc.hasNext()) {
						String token = sc.nextLine().split("\\t")[0];
						this.wordformes.add(token);
						c1++;
					}
					sc.close();
					System.out.println(c1 + " unique types in " + file.getName());
				}
				
				// save verse names in hash set
				if (name.equals("versenames.txt")){
					Scanner sc = new Scanner(file);
					int c2 = 0;
					while (sc.hasNext()) {
						String token = sc.nextLine();
						this.versenames.add(token.trim());
						c2++;
					}
					sc.close();
					System.out.println(c2 + " verses in " + file.getName());
				}	
				
			} // end if loop of isFile
		} // end for loop of files
		
		// save the matrix information as inverted list
		Scanner sc = new Scanner(new File(directory + filename + File.separator + filename + ".mtx"));		
		
		String wordform = "";
		String versename = "";		
		int wordformnumber;
		int versenamenumber;
		
		int wordno = 0;
		int c3 = 0;
		while (sc.hasNext()) {
			String line = sc.nextLine();
			c3++;
			
			// here starts the actual information reading
			if (c3 >= 4){
				String[] lineArray = line.split(" ");
				wordformnumber = Integer.valueOf(lineArray[0]);
				versenamenumber = Integer.valueOf(lineArray[1]);
								
				if (wordformnumber != wordno){
					if (wordno > 0){
						wordform = this.wordformes.get(wordno-1);
						this.invertedList.put(wordform, versename);
					}
					versename = versename.trim();
					System.out.println(wordform + " " + versename);
					versename = this.versenames.get(versenamenumber-1);
				}
				else{
					versename = versename + this.versenames.get(versenamenumber-1) + " ";				
				}
				wordno = wordformnumber;
			}
		}
		sc.close();
		System.out.println(this.invertedList.size() + " verses in " + directory + filename + File.separator + filename + ".mtx");
		
	} // end constructor
	
	
	// make a plain text out of the inverse list data
	public void setPlain(){
		
		// reproduce the plain-ish tex
		for (String wordform : this.invertedList.keySet()) {
			
			// get an array of all verses which contain the word wordform
			String[] verses = this.invertedList.get(wordform).split("\\s+");
			
			// run over all verses of the given word form and probably create a new hash set for each; save then the word form
			HashSet<String> hs = new HashSet<String>();
			for (int i = 0; i < verses.length; i++){
				String verse = verses[i];
				if (this.plainText.containsKey(verse))
					hs = this.plainText.get(verse);
				hs.add(wordform);
				this.plainText.put(verse, hs);
			}		
		}
	}
	
	// get plain Bible from inverse Data
	public HashMap<String, HashSet<String>> getPlain(){		
		return this.plainText;
	}	
	
	
	
}


















