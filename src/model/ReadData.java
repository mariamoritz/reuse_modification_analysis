package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

import model.Bible;

public class ReadData {
	
	private static String slash = File.separator;
	private static String mode;
	private static String dataRoot;
	//private static LinkedList<String> dataDirectories;
	private LinkedList<Bible> dataSet;
	private LinkedList<Bible> dataSetLem;
	private LinkedList<Bible> dataSetNorm;
	private LinkedList<Bible> dataSetPOS;
	private Bible Alignment;
	
	// constructor
	public ReadData(String mode, String datafile) throws FileNotFoundException{
		
		dataSet = new LinkedList<Bible>();
		dataSetLem = new LinkedList<Bible>();
		dataSetNorm = new LinkedList<Bible>();
		dataSetPOS = new LinkedList<Bible>();
		Alignment = new Bible();
		ReadData.mode = mode;
		ReadData.dataRoot = "data" + slash +"in" + slash;
			
		// development
		if (ReadData.mode.equals("development")){
			
			Scanner sc1 = new Scanner(new File(ReadData.dataRoot + datafile));			
			// create the Bible objects
			while (sc1.hasNext()){
				String line = sc1.nextLine();
				String file_string = ReadData.dataRoot + line.split(" ")[0] + slash + line.split(" ")[1] + ".txt";
				
				Bible b = new Bible(file_string);	
				b.setLabel(line.split(" ")[1].split("_")[0]);
				b.setYear(line.split(" ")[1].split("_")[1]);
				dataSet.add(b);
				System.out.println(b.getLabel() + " read.");
				
				// the lem bibs do not have the year info, so it must be cut off to find the corresponding file
				Bible b_lem = new Bible(file_string.substring(0, file_string.length()-9) + "_lem.txt");
				b_lem.setLabel(line.split(" ")[1].split("_")[0] + "_lem");
				b_lem.setYear(line.split(" ")[1].split("_")[1]);
				dataSetLem.add(b_lem);
				System.out.println(b_lem.getLabel() + " read.");		
				
				// the norm bibs do not have the year info, so it must be cut off to find the corresponding file
				Bible b_norm = new Bible(file_string.substring(0, file_string.length()-9) + "_norm.txt");
				b_norm.setLabel(line.split(" ")[1].split("_")[0] + "_norm");
				b_norm.setYear(line.split(" ")[1].split("_")[1]);
				dataSetNorm.add(b_norm);
				System.out.println(b_norm.getLabel() + " read.");	
				
				// the pos bibs do not have the year info, so it must be cut off to find the corresponding file
				Bible b_pos = new Bible(file_string.substring(0, file_string.length()-9) + "_pos.txt");
				b_pos.setLabel(line.split(" ")[1].split("_")[0] + "_pos");
				b_pos.setYear(line.split(" ")[1].split("_")[1]);
				dataSetPOS.add(b_pos);
				System.out.println(b_pos.getLabel() + " read.");	
			}
			sc1.close();							
		}
		
		// server
		/*else if (mode.equals("server")){
			ReadData.dataRoot = slash + "roedel" + slash + "Data" + slash;
						
			Scanner sc1 = new Scanner(new File(ReadData.dataRoot + "files_paralleltext.txt"));
			while (sc1.hasNext()){
				String str = sc1.nextLine();
				String lang = str.split("-")[0];
				ReadData.dataDirectories.add(ReadData.dataRoot + "bibData" + slash + lang + slash + str);
			}
			sc1.close();
			// run through the directories
			for (String folder : ReadData.dataDirectories){
				Bible b = new Bible(new File(folder));
				String name = folder.split(slash)[folder.split(slash).length-1];
				b.setLabel(name);
				b.setPlain();	
				dataSet.add(b);
			}
		} // end server	*/ // not in use	
		
	} // end constructor
	
	public LinkedList<Bible> getDataSet(){
		return this.dataSet;
	}
	
	public LinkedList<Bible> getDataSetLem(){
		return this.dataSetLem;
	}
	
	public LinkedList<Bible> getDataSetNorm(){
		return this.dataSetNorm;
	}
	
	public LinkedList<Bible> getDataSetPOS(){
		return this.dataSetPOS;
	}
	
	public Bible getAlignment(){
		return this.Alignment;
	}
 
		
	
} // end class









