package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

public final class Lexicon {
	
	private static HashSet<HashSet<String>> synonyms = new HashSet<HashSet<String>>();
	private static HashSet<String> synonympairs = new HashSet<String>();
	private static HashMap<HashSet<String>, HashSet<String>> hypernyms = new HashMap<HashSet<String>, HashSet<String>>();
	private static HashSet<String> hypernympairs = new HashSet<String>();
	private static HashMap<HashSet<String>, HashSet<String>> hyponyms = new HashMap<HashSet<String>, HashSet<String>>();
	private static HashSet<String> hyponympairs = new HashSet<String>();
	private static HashSet<HashSet<String>> cohyponyms = new HashSet<HashSet<String>>();
	private static HashSet<HashSet<String>> derivations = new HashSet<HashSet<String>>();
	private static HashMap<String, String> paraphrases = new HashMap<String, String>();
	private static HashMap<String, HashSet<Integer>> paraphraseID = new HashMap<String, HashSet<Integer>>();
	//private static HashMap<Integer, String> IDparaphrase = new HashMap<Integer, String>();
	private static String wordnet_directory = new String();
	private static String paraphrase_directory = new String();
		
	public Lexicon(boolean babelnet, boolean conceptnet, boolean conceptnetpairs, String lang, boolean cohypos, boolean derivation, boolean paraphrases) throws IOException {
		wordnet_directory = "data" + File.separator + "in" + File.separator + "wordnet_" + lang + File.separator;
		paraphrase_directory = "lib" + File.separator + "meteor-1.5" + File.separator + "data" + File.separator + "paraphrase-" + lang + File.separator + "paraphrase-" + lang + "-clean";
		try {
			if (babelnet){
				this.readSynonyms(wordnet_directory + "synsets.txt");
				this.readHypernyms(wordnet_directory + "hypers.txt");
				this.readHyponyms(wordnet_directory + "hypos.txt");
			}
			if (conceptnet){
				if (conceptnetpairs){
					this.readSynpairs(wordnet_directory + "synpairs_CoNe.txt");
					this.readHyperpairs(wordnet_directory + "hyperpairs_CoNe.txt");
					this.readHypopairs(wordnet_directory + "hypopairs_CoNe.txt");
				}					
				else if(!conceptnetpairs){
					this.readSynonyms(wordnet_directory + "synsets_CoNe.txt");
					this.readHypernyms(wordnet_directory + "hypers_CoNe.txt");
					this.readHyponyms(wordnet_directory + "hypos_CoNe.txt");
				}
			}
			if (cohypos){
				if (babelnet)
					this.readCohyponyms(wordnet_directory + "cohypos.txt");
				if (conceptnet)
					this.readCohyponyms(wordnet_directory + "cohypos_CoNe.txt");
			}
			if (derivation)
				this.readDerivation(wordnet_directory + "derivation.txt");
			if (paraphrases)
				this.readParaphrases(paraphrase_directory);
		} catch (FileNotFoundException e) {
			System.out.println("Make sure all lexicon files exist and are in the right encoding.");
			e.printStackTrace();
		}
		System.out.println("Lexicon built.");
	}
	
	// Reads synonyms as hash set of hash sets
	public void readSynonyms(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String tmp = sc.nextLine();
			String[] line = tmp.split("\\s+");
			HashSet<String> value = new HashSet<String>();
			for (int i = 0; i < line.length; i++){
				value.add(line[i].toLowerCase());
			}
			synonyms.add(value);		
		}
		sc.close();
		System.out.println("Synsets read.");
	}
	
	public void readSynpairs(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String line = sc.nextLine();
			synonympairs.add(line);		
		}
		sc.close();
		System.out.println("Synpairs read.");
	}
	
	// Reads hypernyms as hash set of associated hash sets
	public void readHypernyms(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String[] line = sc.nextLine().split("\\t+");			
			String[] ks = line[0].split("\\s+");
			String[] hypers = line[1].split("\\s+");
			HashSet<String> keys = new HashSet<String>();
			HashSet<String> values = new HashSet<String>();
			for (int j = 0; j < hypers.length; j++)
				values.add(hypers[j].toLowerCase());
			for (int i = 0; i < ks.length; i++){
				keys.add(ks[i].toLowerCase());
			}
			hypernyms.put(keys, values);
		}	
		sc.close();
		System.out.println("Hypernyms read.");
	}
	
	public void readHyperpairs(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String line = sc.nextLine();
			hypernympairs.add(line);		
		}
		sc.close();
		System.out.println("Hyperpairs read.");
	}
	
	
	// 	Reads hypernyms as hash set of associated hash sets
	public void readHyponyms(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String[] line = sc.nextLine().split("\\t+");
			//String pos = line[0];		// later????		
			String[] ks = line[0].split("\\s+");  
			String[] hypos = line[1].split("\\s+");  
			HashSet<String> keys = new HashSet<String>();
			HashSet<String> values = new HashSet<String>();
			for (int j = 0; j < hypos.length; j++)
				values.add(hypos[j].toLowerCase());
			for (int i = 0; i < ks.length; i++){
				keys.add(ks[i].toLowerCase());
			}
			hyponyms.put(keys, values);	
		}
		sc.close();
		System.out.println("Hyponyms read.");
	}
	
	public void readHypopairs(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String line = sc.nextLine();
			hyponympairs.add(line);		
		}
		sc.close();
		System.out.println("Hypopairs read.");
	}
	
	// Reads co-hyponyms as hash set of hash sets
	public void readCohyponyms(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String[] line = sc.nextLine().split("\\s+");
			HashSet<String> value = new HashSet<String>();
			for (int i = 0; i < line.length; i++)
				value.add(line[i].toLowerCase());
			cohyponyms.add(value);				
		}	
		sc.close();
		System.out.println("Co-hyponyms read.");
	}
	
	public void readDerivation(String file) throws FileNotFoundException{
		Scanner sc = new Scanner(new File(file));
		while(sc.hasNext()){
			String[] line = sc.nextLine().split("\\s+");
			HashSet<String> value = new HashSet<String>();
			for (int i = 0; i < line.length; i++)
				value.add(line[i].toLowerCase());
			derivations.add(value);				
		}	
		sc.close();
		System.out.println("Derivation dictionary read.");
	}
	
	public void readParaphrases(String file) throws IOException{
		Scanner sc = new Scanner(new File(file));
		int counter = 0;
		int IDcounter = 0;
		String s1 = "";
		String s2 = "";
		boolean init = true;
		BufferedReader br = Files.newBufferedReader(Paths.get(file), StandardCharsets.UTF_8);
		for (String line = null; (line = br.readLine()) != null;) {
		//while(sc.hasNext()){
			if (init){ String s = line; init = false; counter++; continue;}
			counter++;
			if (counter == 2){
				s1 = line; //sc.nextLine();
			}
			else if (counter == 3){
				s2 = line; //sc.nextLine();
			}
			else if (counter == 4){	
				if (s1.length() > 0 && s2.length() > 1){
					counter = 1;
					// save paraphrase
					paraphrases.put(s1, s2);
					paraphrases.put(s2, s1);
					//IDparaphrase.put(IDcounter, s1+"\t"+s2);
					HashSet<Integer> keys = new HashSet<Integer>();
					if (paraphraseID.containsKey(s1))
						 keys = paraphraseID.get(s1);
					keys.add(IDcounter);
					paraphraseID.put(s1, keys);
					keys = new HashSet<Integer>();
					if (paraphraseID.containsKey(s2))
						keys = paraphraseID.get(s2);
					keys.add(IDcounter);
					paraphraseID.put(s2, keys);
					IDcounter++;
				}		
			}				
		}	
		sc.close();
		System.out.println("Paraphrase dictionary read.");
	}
	
	
	public HashSet<HashSet<String>> getSynonyms(){
		return synonyms;
	}
	
	public HashMap<HashSet<String>, HashSet<String>> getHypernyms(){
		return hypernyms;
	}
	
	public HashMap<HashSet<String>, HashSet<String>> getHyponyms(){
		return hyponyms;
	}
	
	public HashSet<HashSet<String>> getCohyponyms(){
		return cohyponyms;
	}
	
	public HashSet<HashSet<String>> getDetivation(){
		return derivations;
	}
	

	// ##################################### Identify Relationship ###########################################
	
	public static boolean areSyns(String w1, String w2) {
		for ( HashSet<String> hs : synonyms ) {
			if ( hs.contains(w1) && hs.contains(w2) )
				return true;
		}
		// this for the conceptNet synonym pair approach
		if ( synonympairs.contains(w1 + " " + w2) || synonympairs.contains(w2 + " " + w1) ){
			return true;
		}
		return false;		
	}
	
	public static boolean areCohypos(String w1, String w2) {
		for ( HashSet<String> hs : cohyponyms ) {
			if ( hs.contains(w1) && hs.contains(w2) )
				return true;
		}
		return false;		
	}
	
	public static boolean hyperOf(String w1, String w2) {
		for ( HashSet<String> hs : hypernyms.keySet() ) {
			if ( hs.contains(w2) && hypernyms.get(hs).contains(w1) )
				return true;
		}
		// the data in the concept net pair files is saved as:  word1 IsA word2, the words in this program are handed over as: (potentialHypernym,word)
		if ( hypernympairs.contains(w2 + "" + w1) ){
			return true;
		}
		return false;
	}
	
	public static boolean hypoOf(String w1, String w2) {
		for ( HashSet<String> hs : hyponyms.keySet() ) {
			if ( hs.contains(w2) && hyponyms.get(hs).contains(w1) )
				return true;
		}
		// the data in the concept net pair files is saved as:  word1 IsFather word2, the words in this program are handed over as: (potentialHyponym,word)
		if ( hyponympairs.contains(w2 + "" + w1) ){
			return true;
		}
		return false;
	}
	
	public static boolean areDerivations(String w1, String w2) {
		for ( HashSet<String> hs : derivations ) {
			if ( hs.contains(w1) && hs.contains(w2) )
				return true;
		}
		return false;		
	}
	
	
	public static ArrayList<Object> findParaphrases(LinkedList<String> words1, LinkedList<String> v1l, LinkedList<String> v1n, LinkedList<String> v1pos, 
			LinkedList<String> words2, LinkedList<String> v2l, LinkedList<String> v2n, LinkedList<String> v2pos) {
		
		ArrayList<Object> ret = new ArrayList<Object>();
		LinkedList<Object> phraseOps = new LinkedList<Object>();
		LinkedList<Object> inputdata = new LinkedList<Object>();
		int[] indices = new int[5];
		indices[0] = 0;
		while(indices[0] != -1){
			indices = getPhaseID(v1n, v2n);
			System.out.println("indices:" + Integer.valueOf(indices[0]));	
			
			if (indices[0] != -1){
				String str1 = "";
				String str2 = "";
				// save the relating phrases
				for (int i = indices[1]; i <= indices[2]; i++)
					str1 = str1 + v1n.get(i) + " ";
				str1 = str1.substring(0,str1.length()-1);
				for (int i = indices[3]; i <= indices[4]; i++)
					str2 = str2 + v2n.get(i) + " ";
				str2 = str2.substring(0,str2.length()-1);
				String o = String.valueOf(indices[1]) + ":" + String.valueOf(indices[3]) + " " + "phrase(" + str1 + "," + str2 + ");";
				phraseOps.add(o);
			
				// update words1 and words2
				int var = indices[2] - indices[1];
				while (var >= 0){
					var--;
					words1.remove(indices[1]); v1l.remove(indices[1]); v1n.remove(indices[1]); v1pos.remove(indices[1]);
				}
				/*for (int i = 0; i <= words1.size(); i++){
					if (i >= indices[1] && i <= indices[2]){
						words1.remove(i);
						v1l.remove(i); v1n.remove(i); v1pos.remove(i);
					}
				}*/
				var = indices[4] - indices[3];
				while (var >= 0){
					var--;
					words2.remove(indices[3]); v2l.remove(indices[3]); v2n.remove(indices[3]); v2pos.remove(indices[3]);
				}
				/*for (int i = 0; i <= words2.size(); i++){
					if (i >= indices[3] && i <= indices[4]){
						words2.remove(i);
						v2l.remove(i); v2n.remove(i); v2pos.remove(i);
					}
				}*/			
			}
			
		}
		// finally, update all 8 strings
		inputdata.add(words1); inputdata.add(v1l); inputdata.add(v1n); inputdata.add(v1pos);
		inputdata.add(words2); inputdata.add(v2l); inputdata.add(v2n); inputdata.add(v2pos);
		ret.add(phraseOps);
		ret.add(inputdata);
		return ret;
	}
	
	
	public static int[] getPhaseID(LinkedList<String> words1, LinkedList<String> words2){
		
		Integer id = -1;
		int[] result = new int[5];
		HashMap<Integer, String> ids_candidate1 = new HashMap<Integer, String>();
		HashMap<Integer, Integer[]> ids_ranges1 = new HashMap<Integer, Integer[]>();
		HashMap<Integer, String> ids_candidate2 = new HashMap<Integer, String>();
		HashMap<Integer, Integer[]> ids_ranges2 = new HashMap<Integer, Integer[]>();
		
		// start with one side of the potential phrase, try to find longest match 1st, then iterate the starting word
		for (int i = 0; i < words1.size(); i++){
			// shorten the phrase candidate
			for (int j = words1.size()-1; j >= 0; j--){
				// i and j are the starting/ending indices of the phrase candidate that is going to be returned
				String cand = "";
				for (int k = i; k <= j; k++){
					cand = cand + words1.get(k) + " ";
				}
				if(cand.length() < 1)
					continue;
				cand = cand.substring(0,cand.length()-1);
				if (paraphrases.keySet().contains(cand)){
					// add all phrase ID for that candidate together with the string; also save the indices of the verse
					for (Integer k : paraphraseID.get(cand)){
						ids_candidate1.put(k, cand);
						Integer[] ia = new Integer[2]; ia[0] = i; ia[1] = j;
						ids_ranges1.put(k, ia);
					}
				}
			}	
		}
		
		// if one side is covered, we need to find the other too
		if (ids_candidate1.size() > 0){
			for (int i = 0; i < words2.size(); i++){
				// shorten the phrase candidate
				for (int j = words2.size()-1; j >= 0; j--){
					// i and j are the starting/ending indices of the phrase candidate that is going to be returned
					// to string
					String cand = "";
					for (int k = i; k <= j; k++){
						cand = cand + words2.get(k) + " ";
					}
					if(cand.length() < 1)
						continue;
					cand = cand.substring(0,cand.length()-1);
					if (paraphrases.keySet().contains(cand)){
						// add all phrase ID for that candidate together with the string; also save the indices of the verse
						for (Integer k : paraphraseID.get(cand)){
							ids_candidate2.put(k, cand);
							Integer[] ia = new Integer[2]; ia[0] = i; ia[1] = j;
							ids_ranges2.put(k, ia);
						}
					}
				}	
			}
			// we have at least one paraphrase match, now we need to find the av. longest
			id = -1;
			for (Integer phid : ids_candidate1.keySet()){
				double av_length = 0;
				// matching IDs but not the identical string
				String p1 = ids_candidate1.get(phid);
				String p2 = ids_candidate2.get(phid);
				if (ids_candidate2.keySet().contains(phid) && !p1.equals(p2)){
					double len = ((double)p1.length()+(double)p2.length())/2.0;
					if (len > av_length){
						av_length = len;
						id = phid;
					}
				}
			}
		}
		if (id != -1){
			result[0] = id; result[1] = ids_ranges1.get(id)[0]; result[2] = ids_ranges1.get(id)[1]; result[3] = ids_ranges2.get(id)[0]; result[4] = ids_ranges2.get(id)[1];
		}
		else{
			result[0] = -1; result[1] = -1; result[2] = -1; result[3] = -1; result[4] = -1;
		}
		return result;
	}
	
	
	// ################################# Identify Relationship Exists #######################################
	
	public boolean syn_contained(String w1) {
		for ( HashSet<String> hs : synonyms ) {
			if ( hs.contains(w1) )
				return true;
		}
		return false;		
	}
	
	public boolean cohypo_contained(String w1) {
		for ( HashSet<String> hs : cohyponyms ) {
			if ( hs.contains(w1) )
				return true;
		}
		return false;		
	}

	public boolean hyper_contained(String w1) {
		for ( HashSet<String> hs : hypernyms.keySet() ) {
			if ( hs.contains(w1) )
				return true;
		}
		return false;
	}

	public boolean hypo_contained(String w1) {
		for ( HashSet<String> hs : hyponyms.keySet() ) {
			if ( hs.contains(w1) )
				return true;
		}
		return false;
	}
	
	public boolean derivation_contained(String w1) {
		for ( HashSet<String> hs : derivations ) {
			if ( hs.contains(w1) )
				return true;
		}
		return false;		
	}

	
	public static void main(String[] args) throws IOException{
		
		Lexicon lex = new Lexicon(false, false, false, "en", false, false, true); 
		LinkedList<String> ll11 = new LinkedList<String>(); LinkedList<String> ll12 = new LinkedList<String>(); LinkedList<String> ll13 = new LinkedList<String>(); LinkedList<String> ll14 = new LinkedList<String>();
		LinkedList<String> ll21 = new LinkedList<String>(); LinkedList<String> ll22 = new LinkedList<String>(); LinkedList<String> ll23 = new LinkedList<String>(); LinkedList<String> ll24 = new LinkedList<String>();
		ll11.add("has"); ll11.add("yet"); ll11.add("emerged");
		ll12.add("has"); ll12.add("yet"); ll12.add("emerged");
		ll13.add("has"); ll13.add("yet"); ll13.add("emerged");
		ll14.add("has"); ll14.add("yet"); ll14.add("emerged");
		ll21.add("is"); ll21.add("still");
		ll22.add("is"); ll22.add("still");
		ll23.add("is"); ll23.add("still");
		ll24.add("is"); ll24.add("still");
		ArrayList<Object> al = Lexicon.findParaphrases(ll11, ll12, ll13, ll14, ll21, ll22, ll23, ll24);
		
		LinkedList<String> ll111 = (LinkedList<String>)al.get(0); // ll11 contains string in form of phrase ops
		LinkedList<Object> ll222 = (LinkedList<Object>)al.get(1); // ll22 contains 8 linkedLists all holding Strings
		for (String o : ll111){
			System.out.println(o);
		}
	}


}
